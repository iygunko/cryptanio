<?php

    namespace App\AdminCustomization;

    use TCG\Voyager\FormFields\AbstractHandler;

    class PreviewImgFormField extends AbstractHandler
    {
        protected $codename = 'preview_img';

        public function createContent($row, $dataType, $dataTypeContent, $options)
        {
            return view('admin.formfields.preview_img', [
                'row' => $row,
                'options' => $options,
                'dataType' => $dataType,
                'dataTypeContent' => $dataTypeContent
            ]);
        }
    }