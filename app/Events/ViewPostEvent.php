<?php

    namespace App\Events;

    use App\Models\Post;
    use Illuminate\Support\Facades\Cookie;

    class ViewPostEvent
    {
        public function handle($post)
        {
            if (!isset($_COOKIE['viewed_post_'.$post->id])) {
                // Increment the view counter by one...
                $post->increment('view_count');
                setcookie('viewed_post_'.$post->id, $post->id, 2147483647);
            }
        }
    }