<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 16.03.2018
     * Time: 0:24
     */

    namespace App\Http\Controllers;

    use App\Models\Category;
    use App\Models\Post;
    use App;
    use function Couchbase\defaultDecoder;

    class CategoryController extends Controller
    {
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $categories = Category::withTranslation(App::getLocale())->with(['posts' => function ($query) {
                $query->where('lang', App::getLocale());
            }])->get();
            $categories = $categories->translate(App::getLocale());

            if(!$categories) {
                abort(404);
            }
            $meta['title'] = 'Categories Cryptanio';
            $meta['description'] = 'Categories Cryptanio';

            return view('pages.categories')->with(['categories' =>  $categories, 'meta' => $meta]);
        }

        /**
         * @param $categorySlug
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function single($categorySlug)
        {
            $category = Category::withTranslation(App::getLocale())->with(['posts' => function ($query) {
                $query->where('lang', App::getLocale());
            }])->where('slug', $categorySlug)->first();
            if (!$category) {
                abort(404, 'Page not found');
            }
            $category = $category->translate(App::getLocale());

            $categoryPosts = Post::with('category')->where(['lang' => App::getLocale()])->where('category_id', $category->id)->orderBy('id', 'desc')->paginate(4);

            $otherPosts = Post::with('category')->where('lang', App::getLocale())->where('category_id', '!=', $category->id)->orderBy('id', 'DESC')->limit(4)->get();

            $meta['title'] = $category->name.' | Category Cryptanio';
            $meta['description'] = $category->name.' | Category Cryptanio';

            return view('pages.category')->with(['meta' => $meta, 'category' => $category, 'categoryPosts' => $categoryPosts, 'otherPosts' => $otherPosts]);
        }
    }