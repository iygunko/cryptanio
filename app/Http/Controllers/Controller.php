<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use App\Models\Ico;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $futureIcos = Ico::with('category')->where('status','Готовится')->limit(5)->get();
        $currentIcos = Ico::with('category')->where('status','Проходит')->limit(5)->get();
        $pastIcos = Ico::with('category')->where('status','Завершен')->limit(5)->get();
        $sidebarIcos = ['futureIcos'=> $futureIcos, 'currentIcos' => $currentIcos, 'pastIcos' => $pastIcos];

        View::share(['sidebarIcos' => $sidebarIcos]);
    }
}
