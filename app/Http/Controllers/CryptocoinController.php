<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 10.04.2018
     * Time: 22:24
     */

    namespace App\Http\Controllers;

    use App\Models\Category;
    use App\Models\Cryptocoin;
    use App\Models\Exchanger;

    class CryptocoinController extends Controller
    {
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            $cryptocoins = (isset($_GET['all'])) ? Cryptocoin::all() : Cryptocoin::latest()->simplePaginate(2);

            $exchangers = Exchanger::limit(20)->get();

            $meta['title'] = 'Cryptocoins Cryptanio';
            $meta['description'] = 'Cryptocoins Cryptanio';

            return view('pages.cryptocoins', ['meta' => $meta, 'cryptocoins' => $cryptocoins, 'exchangers' => $exchangers]);
        }

        /**
         * @param $cryptocoinSlug
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function single($cryptocoinSlug)
        {
            $cryptocoin = Cryptocoin::where('slug', '=', $cryptocoinSlug)->first();

            $relatedCategories = Category::with('posts')->limit(2)->get();
            $meta['title'] = $cryptocoin->name.' | Cryptocoin Cryptanio';
            $meta['description'] = $cryptocoin->name.' | Cryptocoin Cryptanio';

            return view('pages.cryptocoin_single',['meta' => $meta, 'relatedCategories' => $relatedCategories, 'cryptocoin' => $cryptocoin]);
        }
    }