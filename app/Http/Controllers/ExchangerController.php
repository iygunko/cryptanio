<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 05.05.2018
 * Time: 18:06
 */

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Exchanger;
use App;
use Illuminate\Http\Request;

class ExchangerController  extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $doExchange = [];
        if (isset($request['from_currency']) && $request['to_currency']) {
            $doExchange['from_currency'] = $request['from_currency'];
            $doExchange['to_currency'] = $request['to_currency'];
        }
        $exchangers = Exchanger::where('is_paysystem', '=', 0)->paginate(5);
        $paysystems = Exchanger::where('is_paysystem', '=', 1)->paginate(5);

        $meta['title'] = 'Exchangers Cryptanio';
        $meta['description'] = 'Exchangers Cryptanio';

        $bottomCategories = Category::withTranslation(App::getLocale())->where('order_by_home', '!=',null)->with('posts')->orderBy('order_by_home', 'asc')->get();
        $bottomCategories = $bottomCategories->translate(App::getLocale());

        return view('pages.buy_sell', compact(['paysystems', 'meta', 'exchangers', 'bottomCategories', 'doExchange']));
    }
}