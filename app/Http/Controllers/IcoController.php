<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 22.04.2018
 * Time: 21:48
 */

namespace App\Http\Controllers;

use App\Models\Ico;
use Illuminate\Http\Request;
use App;
use App\Models\Post;
use App\Models\Category;

class IcoController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $futureIcos = Ico::with('category')->where('status','Готовится')->paginate(1);
        $currentIcos = Ico::with('category')->where('status','Проходит')->paginate(1);
        $pastIcos = Ico::with('category')->where('status','Завершен')->paginate(1);

        $meta['title'] = 'Icos Cryptanio';
        $meta['description'] = 'Icos Cryptanio';

        $newPosts = Post::with('category')->withTranslation(App::getLocale())->latest()->take(4)->get();
        $newPosts = $newPosts->translate(App::getLocale());

        return view('pages.icos', ['meta' => $meta, 'futureIcos' => $futureIcos, 'currentIcos' => $currentIcos, 'pastIcos' => $pastIcos, 'newPosts' => $newPosts]);
    }

    /**
     * @param $icoSlug
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function single($icoSlug)
    {
        $ico = Ico::with('category')->where('slug', $icoSlug)->first();
        if(!$ico) {
            abort(404, 'Page not found');
        }
        $newPosts = Post::with('category')->where('lang', App::getLocale())->latest()->take(4)->get();

        $meta['title'] = $ico->name.' | Ico Cryptanio';
        $meta['description'] = $ico->name.' | Ico Cryptanio';

        return view('pages.ico_single', ['meta' => $meta, 'ico' => $ico, 'newPosts' => $newPosts]);
    }
}