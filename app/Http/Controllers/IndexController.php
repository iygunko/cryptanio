<?php
	/**
	 * Created by PhpStorm.
	 * User: Iva
	 * Date: 16.03.2018
	 * Time: 0:24
	 */

	namespace App\Http\Controllers;

    use App;
	use App\Models\Category;
    use App\Models\Post;
    use Illuminate\Http\Request;

    class IndexController extends Controller
	{
        /**
         * @param Request $request
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
		public function index(Request $request)
        {
//		    $homeNews = Post::withTranslation(App::getLocale())->orderBy('id', 'desc')->limit(7)->get();
//		    $homeNews = $homeNews->translate(App::getLocale());
            $homeNews = Post::where('lang', App::getLocale())->orderBy('id', 'desc')->limit(7)->get();

		    $homeCategories = Category::withTranslation(App::getLocale())->where('order_by_home', '!=',null)->with(['posts' => function ($query) {
                $query->where('lang', App::getLocale());
            }])->orderBy('order_by_home', 'asc')->get();
            $homeCategories = $homeCategories->translate(App::getLocale());

            $top5Cryptocoins = App\Models\Cryptocoin::orderBy('home_order_by')->limit(5)->get();

			return view('pages.homepage')->with(
			    ['top5Cryptocoins' => $top5Cryptocoins, 'homeNews' => $homeNews, 'homeCategories' => $homeCategories]);
		}
	}