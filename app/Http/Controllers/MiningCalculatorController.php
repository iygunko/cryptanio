<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 08.04.2018
     * Time: 21:37
     */

    namespace App\Http\Controllers;

    use App\Models\Category;
    use App\Models\Post;
    use App\Models\Course;
    use App;

    class MiningCalculatorController extends Controller
    {
        /**
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index()
        {
            //id(9) is mining
            $miningCategory = Category::withTranslation(App::getLocale())->with('posts')->where('id', '=','9')->first();
            $miningCategory = $miningCategory->translate(App::getLocale());

            $otherPosts = Post::where('lang', App::getLocale())->with('category')->where('category_id', '!=', $miningCategory->id)->orderBy('id', 'DESC')->limit(4)->get();

//            $otherPosts = Post::withTranslation(App::getLocale())->with('category')->where('category_id', '!=', $miningCategory->id)->orderBy('id', 'DESC')->limit(4)->get();
//            $otherPosts = $otherPosts->translate(App::getLocale());

            $meta['title'] = $miningCategory->name.' | Mining Cryptanio';
            $meta['description'] = $miningCategory->name.' | Mining Cryptanio';

            return view('pages.mining')->with(compact(["meta", "miningCategory", "otherPosts"]));
        }
    }