<?php

    namespace App\Http\Controllers;

    use App\Models\Post;
    use App\Events\ViewPostEvent;
    use App;
    use Illuminate\Http\Request;

    class PostController extends Controller
    {
        CONST SEARCHED_POSTS_PER_PAGE = 30;

        /**
         * @param $categorySlug
         * @param $postSlug
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function index($categorySlug, $postSlug)
        {
//            $post = Post::withTranslation(App::getLocale())->where('slug', $postSlug)->with('category')->firstOrFail();
            $post = Post::where('slug', $postSlug)->where('lang', App::getLocale())->with('category')->first();

            if (!$post) {
                abort(404, 'Page not found');
            }
            event((new ViewPostEvent())->handle($post));
//            $post = $post->translate(App::getLocale());

            $meta['title'] = $post->name.' | Blog Cryptanio';
            $meta['description'] = $post->name.' | Blog Cryptanio';

            $newPosts = Post::with('category')->where('lang',  App::getLocale())->latest()->take(4)->get();
//            $newPosts = Post::with('category')->withTranslation(App::getLocale())->latest()->take(4)->get();
//            $newPosts = $newPosts->translate(App::getLocale());

            return view('pages.post_single')->with(['meta' => $meta, 'post' => $post, 'newPosts' => $newPosts]);
        }

        /**
         * @param Request $request
         *
         * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function search(Request $request)
        {
//            $searchedPosts =  Post::withTranslation(App::getLocale())->where('title', 'LIKE', '%' . $request['search'] . '%')->orderBy('id', 'asc')->paginate(self::SEARCHED_POSTS_PER_PAGE);
//            $searchedPosts = $searchedPosts->translate(App::getLocale());
            $searchedPosts =  Post::where('lang', App::getLocale())->where('title', 'LIKE', '%' . $request['search'] . '%')->orderBy('id', 'asc')->paginate(self::SEARCHED_POSTS_PER_PAGE);

            return view('pages.search')->with(["searchedPosts" => $searchedPosts]);
        }
    }