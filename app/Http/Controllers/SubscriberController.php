<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 28.03.2018
     * Time: 23:47
     */

    namespace App\Http\Controllers;

    use App\Models\Subscriber;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Redirect;

    class SubscriberController extends Controller
    {
        /**
         * @param Request $request
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function addNewSubscriber(Request $request)
        {
            $inputs = $request->all();
            if (!isset($inputs['email']) || $inputs['email'] == '') {
                \Session::flash('error', 'You were not subscribe! Please check input data.' );
                return Redirect::back();
            }

            $result = Subscriber::create(['email' => $inputs['email']]);
            if ($result) {
                \Session::flash('msg', 'You are successfully subscribed!' );
                return Redirect::back();
            } else {
                \Session::flash('error', 'You were not subscribe! Please check input data.' );
                return Redirect::back();
            }
        }
    }