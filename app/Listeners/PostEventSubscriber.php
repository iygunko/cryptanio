<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 20.03.2018
     * Time: 23:41
     */

    namespace App\Listeners;

    class PostEventSubscriber
    {
        /**
         * Handle user login events.
         */
        public function onUserLogin($event) {}

        /**
         * Handle user logout events.
         */
        public function onUserLogout($event) {}

        /**
         * Register the listeners for the subscriber.
         *
         * @param  Illuminate\Events\Dispatcher  $events
         */
        public function subscribe($events)
        {
            $events->listen(
                'App\Events\ViewPostEvent',
                'App\Listeners\PostEventSubscriber@handle'
            );
        }
    }