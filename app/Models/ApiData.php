<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 09.04.2018
     * Time: 19:30
     */

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;
    use Config;

    class ApiData extends Model
    {
        //coinwarz.com api key
        const COINWARZ_API_KEY = '74a012f9edc54687a44d68bfdf535269';

        public function getBitcoinDifficulty()
        {
            $params = [
                'q' => 'getDifficulty'
            ];
            $ch = curl_init("https://blockexplorer.com/api/status?q=getDifficulty");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $response = curl_exec($ch);
            curl_close($ch);
            var_dump($response);
            die;
        }

        public function getCoinData($coinCode)
        {
            $availableCodes = Config::get('api.available_crypto_codes');
            if (in_array($coinCode, $availableCodes)) {
                $ch = curl_init("http://www.coinwarz.com/v1/api/coininformation/?apikey=".self::COINWARZ_API_KEY."&cointag=$coinCode");
                $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13';
                curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION , 1);
                curl_setopt($ch, CURLOPT_REFERER , 'https://cryptanio.com');
                curl_setopt($ch,CURLOPT_POST, 0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);
                curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $response = curl_exec($ch);
                curl_close($ch);
                echo "<pre>";
                var_dump($response);
                echo "</pre>";
                die;
            } else {
                throw new \Exception('Incorrect coin Code.');
            }
        }
    }