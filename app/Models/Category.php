<?php

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Category extends Model
    {
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'parent_id', 'order', 'name', 'slug', 'preview_img', 'order_by_home'
        ];
    }
