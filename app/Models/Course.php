<?php

namespace App\Models;

class Course extends \TCG\Voyager\Models\Category
{
    protected $table = 'courses';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'price'
    ];


}
