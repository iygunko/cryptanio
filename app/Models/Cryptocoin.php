<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 26.04.2018
 * Time: 0:58
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Facades\Voyager;


class Cryptocoin extends Model
{
    use HasRelationships;

    protected $table = 'cryptocoins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'icon', 'price', 'old_price', 'low_price_24h', 'high_price_24h', 'trading_volume',
        'description', 'market_cap', 'circulating_supply', 'price_graph_7d', 'home_order_by'
    ];

}