<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Facades\Voyager;

class Exchanger extends Model
{
    use HasRelationships;

    protected $table = 'exchangers';

    /*
     * disable updated_at timestamp
     */
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'site_url', 'name', 'status', 'courses_count', 'bl', 'ts', 'positive_views_count',
        'negative_views_count', 'reserved_sum', 'is_paysystem'
    ];
}