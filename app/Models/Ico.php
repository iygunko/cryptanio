<?php
/**
 * Created by PhpStorm.
 * User: Iva
 * Date: 22.04.2018
 * Time: 18:14
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Facades\Voyager;

class Ico extends Model
{
    //use Translatable, HasRelationships;
    use HasRelationships;

    protected $table = 'icos';

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}