<?php
    /**
     * Created by PhpStorm.
     * User: Iva
     * Date: 28.03.2018
     * Time: 23:39
     */

    namespace App\Models;

    use Illuminate\Database\Eloquent\Model;

    class Subscriber extends Model
    {
        /*
         * disable updated_at timestamp
         */
        const UPDATED_AT = null;
        /**
         * The attributes that are mass assignable.
         *
         * @var array
         */
        protected $fillable = [
            'email'
        ];
    }