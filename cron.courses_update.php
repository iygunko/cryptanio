<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

$config = [
    'db' => [
        'host' => 'localhost:3306',
        'database' => 'sokurevo_access',
        'user' => 'sokurevo_access',
        'password' => 'mhV%5JLT',
        'charset' => 'utf8'
    ]
];

/**
 * Class Db
 */
class Db
{
    private static $status = false;
    private static $db;

    /**
     * @return \PDO $db
     */
    public static function getConnection()
    {
        global $config;

        if (!self::$status) {
            try {
                self::$db = new \PDO("mysql:host=" . $config["db"]["host"] . ";dbname=" . $config["db"]["database"],
                    $config["db"]["user"], $config["db"]["password"]);
                self::$db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
                self::$db->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
                self::$db->exec("set names " . $config["db"]["charset"]);
            } catch (\PDOException $e) {
                echo $e->getMessage();
            }
            self::$status = 1;
        }

        return self::$db;
    }
}

$neededCurrencies = [
    'btc', 'eth', 'xrp', 'ltc', 'bch', 'neo', 'ada', 'xmr', 'dash'
];

/**
 * @param $currencyCodeFrom
 * @param $currencyCodeTo
 *
 * @return null|array
 */
function getCurrencyInfo($currencyCodeFrom, $currencyCodeTo)
{
    $currencyCodeFrom = strtoupper($currencyCodeFrom);
    $currencyCodeTo = strtoupper($currencyCodeTo);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, "https://min-api.cryptocompare.com/data/pricemultifull?fsyms=$currencyCodeFrom&tsyms=$currencyCodeTo");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    $decodedResult = json_decode($result);
    $decodedResultArray =  json_decode(json_encode($decodedResult), true);

    $currencyInfo = isset($decodedResultArray['RAW'][$currencyCodeFrom][$currencyCodeTo]) ? $decodedResultArray['RAW'][$currencyCodeFrom][$currencyCodeTo] : null;

    return $currencyInfo;
}

/**
 * @param $currencyInfo
 *
 * @return bool
 */
function insertFreshCurrencyCourse($currencyInfo)
{
    $db = Db::getConnection();

    $insertSql = $db->prepare("INSERT INTO `courses` 
    (code, to_currency, price, last_update, last_volume, last_trade_id, volumeday, volumedayto, volume24hour,
     volume24hourto, openday, highday, lowday, open24hour, high24hour, low24hour, lastmarket, change24hour, change_pct24_hour,
     changeday, changepctday, supply, mktcap, totalvolume24h, totalvolume24hto)  VALUES 
     (:code, :to_currency, :price, :last_update, :last_volume, :last_trade_id, :volumeday, :volumedayto, :volume24hour,
     :volume24hourto, :openday, :highday, :lowday, :open24hour, :high24hour, :low24hour, :lastmarket, :change24hour, :change_pct24_hour,
     :changeday, :changepctday, :supply, :mktcap, :totalvolume24h, :totalvolume24hto)");
    $insertSql->bindParam(':code', $currencyInfo['FROMSYMBOL'], PDO::PARAM_STR);
    $insertSql->bindParam(':to_currency', $currencyInfo['TOSYMBOL'], PDO::PARAM_STR);
    $insertSql->bindParam(':price', $currencyInfo['PRICE'], PDO::PARAM_STR);
    $insertSql->bindParam(':last_update', $currencyInfo['LASTUPDATE'], PDO::PARAM_STR);
    $insertSql->bindParam(':last_volume', $currencyInfo['LASTVOLUME'], PDO::PARAM_STR);
    $insertSql->bindParam(':last_trade_id', $currencyInfo['LASTTRADEID'], PDO::PARAM_STR);
    $insertSql->bindParam(':volumeday', $currencyInfo['VOLUMEDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':volumedayto', $currencyInfo['VOLUMEDAYTO'], PDO::PARAM_STR);
    $insertSql->bindParam(':volume24hour', $currencyInfo['VOLUME24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':volume24hourto', $currencyInfo['VOLUME24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':openday', $currencyInfo['OPENDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':highday', $currencyInfo['HIGHDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':lowday', $currencyInfo['LOWDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':open24hour', $currencyInfo['OPEN24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':high24hour', $currencyInfo['HIGH24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':low24hour', $currencyInfo['LOW24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':lastmarket', $currencyInfo['LASTMARKET'], PDO::PARAM_STR);
    $insertSql->bindParam(':change24hour', $currencyInfo['CHANGE24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':change_pct24_hour', $currencyInfo['CHANGEPCT24HOUR'], PDO::PARAM_STR);
    $insertSql->bindParam(':changeday', $currencyInfo['CHANGEDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':changepctday', $currencyInfo['CHANGEPCTDAY'], PDO::PARAM_STR);
    $insertSql->bindParam(':supply', $currencyInfo['SUPPLY'], PDO::PARAM_STR);
    $insertSql->bindParam(':mktcap', $currencyInfo['MKTCAP'], PDO::PARAM_STR);
    $insertSql->bindParam(':totalvolume24h', $currencyInfo['TOTALVOLUME24H'], PDO::PARAM_STR);
    $insertSql->bindParam(':totalvolume24hto', $currencyInfo['TOTALVOLUME24HTO'], PDO::PARAM_STR);

    return $insertSql->execute();
}

/**
 * @param $currencies
 */
function updateCurrenciesCourses($currencies)
{
    foreach($currencies as $currencyName) {
        $currencyInfo = getCurrencyInfo(strtoupper($currencyName), 'USD');
        insertFreshCurrencyCourse($currencyInfo);
    }
}

updateCurrenciesCourses($neededCurrencies);