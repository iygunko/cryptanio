/**
 * Created by No Fear on 28.01.2018.
 */

$('.top-lang div').click(function () {
    $(this).siblings('ul').stop().fadeToggle(300);
});

$('.top-menu > li > span').click(function () {
    if($(this).parent().hasClass('open')){
        $(this).siblings('.top-submenu-block').stop().slideUp(300);
        $(this).parent().removeClass('open');
    }
    else {
        $('.top-menu > li').removeClass('open');
        $('.top-submenu-block').stop().slideUp(300);
        $(this).siblings('.top-submenu-block').stop().slideDown(300);
        $(this).parent().addClass('open');
    }
});

$("body").click(function(a){
    if(!$(a.target).parentsUntil('.top-lang').parent().hasClass('top-lang') && !$(a.target).parent().hasClass('top-lang')){
        $(".top-lang ul").stop().fadeOut(300);
    }

    if(!$(a.target).parentsUntil('.top-menu').parent().hasClass('top-menu') && !$(a.target).parent().hasClass('top-menu')){
        $('.top-menu > li').removeClass('open');
        $('.top-submenu-block').stop().slideUp(300);
    }
});

$('.top-menu-btn').click(function () {
    $('.bottom-head, .top-head').addClass('active');
    $('.mob-backdrop').stop().fadeIn(300);
});

$('.top-search-btn').click(function () {
    $('.top-search').stop().fadeIn(300);
});

$('.top-search-backdrop').click(function () {
    $('.top-search').stop().fadeOut(300);
});

$('.mob-backdrop').click(function () {
    $(this).stop().fadeOut(300);
    $('.bottom-head, .top-head').removeClass('active');
});

$(window).scroll(function () {
    if ($(this).scrollTop() > 160) {
        $('.bottom-head').addClass("bottom-head-fixed");
    } else {
        $('.bottom-head').removeClass("bottom-head-fixed");
    }

    if ($(this).scrollTop() > 60) {
        $('.top-head').addClass("top-head-fixed");
    } else {
        $('.top-head').removeClass("top-head-fixed");
    }
}).resize(function () {
    $('.bottom-head, .top-head').removeClass('active');
    $('.mob-backdrop').fadeOut(300);
    $('.top-menu > li').removeClass('open');
    $('.top-submenu-block').slideUp(300);
    $(".top-lang ul").fadeOut(300);
    $('.top-search').fadeOut(300);
});






