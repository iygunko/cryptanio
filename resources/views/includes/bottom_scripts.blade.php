
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('js/jquery.timeago.js')}}"></script>
@if(App::getLocale() == 'ru')
<script src="{{asset('js/jquery.timeago.ru.js')}}"></script>
@elseif(App::getLocale() == 'ua')
<script src="{{asset('js/jquery.timeago.uk.js')}}"></script>
@endif
<script>
    jQuery(document).ready(function() {
        jQuery("time.timeago").timeago();
    });
</script>
@if(Route::is('home'))
<script>
    function main_news_height() {
        var highestBox = 0;
        $('.main-news-list').each(function(){
            $(this).height('auto');
            if($(this).height() > highestBox) {
                highestBox = $(this).height();
            }
        }).height(highestBox);
    }
    function iframeHeight() {
        $('.main-page-table-info-row .right > div').each(function () {
            var that = $(this);
            $(that).height(0);
            $(that).find('iframe').height(0);
            $(that).height($(that).closest('.main-page-table-info-row').find('.left').height());
            $(that).find('iframe').attr('style', 'padding: 0').height($(that).closest('.main-page-table-info-row').height() + 41);
            $(that).closest('.main-table-info').css('padding-bottom','3px');
        });
    }
    $(document).ready(function () {
        var csrfToken = $('meta[name="csrf-token"]').attr('content');
        $('form').append("<input type='hidden' name='_token' value='"+csrfToken+"' />");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        main_news_height();

        $('.main-page-table-wrap tbody tr').not('.main-page-table-info-row').click(function () {
            if($(this).hasClass('active')){
                $(this).removeClass('active');
                $('.main-page-table-info-row').stop().hide(500);
            }
            else{
                $('.main-page-table-wrap tbody tr').removeClass('active');
                $(this).addClass('active');
                $('.main-page-table-info-row').stop().hide(500);
                $(this).next('.main-page-table-info-row').stop().show(500);
                iframeHeight();
            }
        });
    });
    $(window).resize(function () {
        main_news_height();
        iframeHeight();
    });
</script>
@endif



