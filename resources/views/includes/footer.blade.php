<footer class="footer">
    <div class="container">
        <a href="#" class="foot-logo">
            <img src="{{asset('img/logo.png')}}" alt="Cryptanio">
        </a>
        <form action="/subscribe" method="post">
            <p>{{ __('app.get_updates_on_email') }}</p>
            <input type="email" name="email" placeholder="E-mail" class="input-lg" required>
            <input type="submit" name="submit"  class="btn btn-default btn-lg" value='{{ __('app.subscribe') }}'/>
        </form>
        <div class="clearfix"></div>
        <hr>
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-lg-3">
                <?= menu('footer_column_1', 'includes.footer_menu_1'); ?>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3">
                <?= menu('footer_column_2', 'includes.footer_menu_2'); ?>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-3">
                <a href="mailto:info@cryptanio.com" class="foot-feedback"><i class="fa fa-envelope-o"  aria-hidden="true"></i>info@cryptanio.com</a>
            </div>
        </div>
        <div class="bottom-foot">
            <p>© {{ date('Y') }} Cryptanio.com | All rights reserved.</p>
            <ul class="foot-soc">
                <li>
                    <a href="#">
                        <img src="{{asset('img/foot-vk-1.png')}}" alt="vkontakte">
                        <img src="{{asset('img/foot-vk-2.png')}}" alt="vkontakte">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="{{asset('img/foot-fb-1.png')}}" alt="facebook">
                        <img src="{{asset('img/foot-fb-2.png')}}" alt="facebook">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="{{asset('img/foot-tw-1.png')}}" alt="twitter">
                        <img src="{{asset('img/foot-tw-2.png')}}" alt="twitter">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</footer>