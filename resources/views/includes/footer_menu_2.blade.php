<?php
    $items = $items->translate(App::getLocale());
?>
<ul class="foot-menu">
    @foreach($items as $menu_item)
        <li>
            <a href="{{ $menu_item->link() }}">{{ $menu_item->title }}</a>
        </li>
    @endforeach
</ul>