<head>
    <title>{!! isset($meta['title']) ? $meta['title'] :  'Cryptanio' !!}</title>
    <meta name="description" content="{!! isset($meta['description']) ? $meta['description'] :  'Cryptanio' !!}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <!--Facebook share-->
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{!! isset($meta['title']) ? $meta['title'] :  'Cryptanio' !!}"/>
    <meta property="og:description" content="{!! isset($meta['description']) ? $meta['description'] :  'Cryptanio' !!}"/>
    <meta property="og:image" content="="/>
    <meta property="og:image:width" content="760"/>
    <meta property="og:image:height" content="500"/>
    <meta property="og:url" content="="/>
    <!--Twitter-->
    <meta name="twitter:card" content="Cryptanio">
    <meta name="twitter:title" content="{!! isset($meta['title']) ? $meta['title'] :  'Cryptanio' !!}">
    <meta name="twitter:description" content="{!! isset($meta['description']) ? $meta['description'] :  'Cryptanio' !!}">
    <meta name="twitter:image" content="=">

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-theme.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css?1038')}}">
    <link rel="stylesheet" href="{{asset('css/response.css?1006')}}">
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&amp;subset=cyrillic" rel="stylesheet">
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>

    @if(Route::is('buy-sell'))
        <style>
            .buy-table tr td:nth-of-type(1):before {
                content: 'Обменник';
            }

            .buy-table tr td:nth-of-type(2):before {
                content: 'Статус';
            }

            .buy-table tr td:nth-of-type(3):before {
                content: 'Резервы';
            }

            .buy-table tr td:nth-of-type(4):before {
                content: 'Курсов';
            }

            .buy-table tr td:nth-of-type(5):before {
                content: 'BL';
            }

            .buy-table tr td:nth-of-type(6):before {
                content: 'TS';
            }

            .buy-table tr td:nth-of-type(7):before {
                content: 'Отзывы';
            }
        </style>
    @endif
    @if(Route::is('icos'))
        <style>
            .ico-table .item td:nth-of-type(2):before {
                content: 'Название:';
            }
            .ico-table .item td:nth-of-type(3):before {
                content: 'Цена токена:';
            }
            .ico-table .item td:nth-of-type(4):before {
                content: 'До старта:';
            }
            .ico-table .item td:nth-of-type(5):before {
                content: 'Статус:';
            }
            .ico-table .item td:nth-of-type(6):before {
                content: 'Цель:';
            }
        </style>
    @endif

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="">
</head>
