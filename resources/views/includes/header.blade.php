<header class="header">
    <div class="top-head">
        <div class="container">
            <a href="/" class="logo">
                <img src="{{asset('img/logo.png')}}" alt="Cryptanio">
            </a>
            <button type="button" class="top-menu-btn"><i class="fa fa-bars" aria-hidden="true"></i></button>
            <div class="top-lang">
            @if(Session::has('locale'))
                <div>{{ Session::get('locale') }}<i class="fa fa-angle-down" aria-hidden="true"></i></div>
            @else
                <div>{{ App::getLocale() }}<i class="fa fa-angle-down" aria-hidden="true"></i></div>
            @endif
                <ul>
                    <li>
                        <a href="/setlocale/en">Eng</a>
                    </li>
                    <li>
                        <a href="/setlocale/ru">Rus</a>
                    </li>
                    <li>
                        <a href="/setlocale/ua">Ukr</a>
                    </li>
                </ul>
            </div>
            <button type="button" class="top-search-btn"><img src="{{asset('img/top-search-eye.png')}}" alt="search eye"></button>
            <form class="top-search" autocomplete="off" method="get" action="{{ route('search') }}">
                <div class="top-search-backdrop"></div>
                <div class="top-search-wrapper">
                    <input type="search" name="search" placeholder="{{ __('app.search') }}" class="input-sm">
                    <input type="submit" value="">
                </div>
            </form>
        </div>
    </div>
    <div class="bottom-head">
        <nav class="container">
            <ul class="top-menu">
                <li class="fixed-logo">
                    <a href="#">
                        <img src="{{asset('img/logo.png')}}" alt="Cryptanio">
                    </a>
                </li>
                <?= menu('main_menu', 'includes.main_menu'); ?>
            </ul>
        </nav>
    </div>
</header>