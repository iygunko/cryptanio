<?php
    $items = $items->translate(App::getLocale());
?>
@foreach($items as $menu_item)
    <li>
        @if(count($menu_item->children) > 0)
            <span>{{ $menu_item->title }}</span>
        @else
            <a href="{{ $menu_item->link() }}">{{ $menu_item->title  }}</a>
        @endif

        @if(count($menu_item->children) > 0)
            <div class="top-submenu-block">
                <ul class="top-submenu">
                    @foreach($menu_item->children as $sub_menu_item)
                        <li><a href="{{ $sub_menu_item->link() }}">{{  $sub_menu_item->title }}</a></li>
                    @endforeach
                </ul>
            </div>
        @endif
    </li>
@endforeach
