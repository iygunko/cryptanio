<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" prefix="og: http://ogp.me/ns#">

@include("includes.head")

<body>

<div class="top-info-block">
    <!-- TradingView Widget BEGIN -->
    <script src="https://s3.tradingview.com/external-embedding/embed-widget-tickers.js">{
            "symbols": [
                {
                    "description": "BTC / USD",
                    "proName": "BITFINEX:BTCUSD"
                },
                {
                    "description": "ETH / USD",
                    "proName": "BITFINEX:ETHUSD"
                },
                {
                    "description": "XRP / USD",
                    "proName": "BITFINEX:XRPUSD"
                },
                {
                    "description": "LTC / USD",
                    "proName": "BITFINEX:LTCUSD"
                },
                {
                    "description": "BCH / USD",
                    "proName": "BITFINEX:BCHUSD"
                },
                {
                    "description": "NEO / USD",
                    "proName": "BITFINEX:NEOUSD"
                },
                {
                    "description": "ADA / BTC",
                    "proName": "BITTREX:ADABTC"
                },
                {
                    "description": "XMR / USD",
                    "proName": "KRAKEN:XMRUSD"
                },
                {
                    "description": "DASH / USD",
                    "proName": "KRAKEN:DASHUSD"
                },
                {
                    "description": "XLM / BTC",
                    "proName": "BINANCE:XLMBTC"
                },
                {
                    "description": "TRX / USD",
                    "proName": "BITFINEX:TRXUSD"
                },
                {
                    "description": "XEM / BTC",
                    "proName": "POLONIEX:XEMBTC"
                },
                {
                    "description": "BTG / BTC",
                    "proName": "BINANCE:BTGBTC"
                },
                {
                    "description": "IOTA / USD",
                    "proName": "BITFINEX:IOTUSD"
                },
                {
                    "description": "XVG / BTC",
                    "proName": "BITTREX:XVGBTC"
                }
            ],
                "locale": "ru"
        }</script>
    <!-- TradingView Widget END -->
</div>
@include("includes.header")

@if(Session::has('msg'))
    <div class="alert alert-info" style="text-align: center;">
        <strong> {!!Session::get('msg')!!}</strong>
    </div>
@endif
@if(Session::has('error'))
    <div class="alert alert-error" style="text-align: center;">
        <strong> {!!Session::get('error')!!}</strong>
    </div>
@endif

@yield("body")


<div class="mob-backdrop"></div>
@include("includes.footer")
@include("includes.bottom_scripts")

</body>

</html>