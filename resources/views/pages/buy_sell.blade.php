@extends("layout")
@section("body")
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{{ __('app.homepage') }}</a></li>
            <li class="active">{{ __('app.buy-sell_breadcrumb') }}</li>
        </ol>
        <div class="row buy-sell filter-wrapper">
            <div class="col-md-8">
                <button type="button" class="btn-filter-show hidden-lg hidden-md"><i class="fa fa-filter"
                                                                                     aria-hidden="true"></i></button>
                <h2>{{ __('app.buy') }} / {{ __('app.sell') }}</h2>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">{{ __('app.exchangers') }}<i class="fa fa-angle-down"
                                                                                      aria-hidden="true"></i></a></li>
                    <li><a href="#tab-2" data-toggle="tab">{{ __('app.paysystems') }}<i class="fa fa-angle-down"
                                                                               aria-hidden="true"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="tab-1">
                        <table class="buy-table">
                            <thead>
                            <tr>
                                <th>{{ __('app.name') }}</th>
                                <th>{{ __('app.status') }}</th>
                                <th>{{ __('app.reserves') }}</th>
                                <th>{{ __('app.courses') }}</th>
                                <th>BL</th>
                                <th>TS</th>
                                <th>{{ __('app.reviews') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($exchangers as $exchanger)
                            <tr>
                                <td><a href="{!! $exchanger->site_url !!}" target="_blank">{!! $exchanger->name !!}</a></td>
                                <td class="text-{!! ($exchanger->status == 'Active')?'high':'low' !!}">{!! $exchanger->status !!}</td>
                                <td>{!! isset($exchanger->reserves_sum)?'$'.$exchanger->reserves_sum:'-' !!}</td>
                                <td>{!! isset($exchanger->cources_count)?:'-' !!}</td>
                                <td>{!! isset($exchanger->bl)?$exchanger->bl:'-' !!}</td>
                                <td>{!! isset($exchanger->ts)?$exchanger->ts:'-' !!}</td>
                                <td><a href="{!! $exchanger->site_url !!}" target="_blank"><span class="text-low">{!! $exchanger->negative_views_count !!}</span>/<span class="text-high">{!! $exchanger->positive_views_count !!}</span></a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $exchangers->links() }}

                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <table class="buy-table">
                            <thead>
                            <tr>
                                <th>{{ __('app.name') }}</th>
                                <th>{{ __('app.status') }}</th>
                                <th>{{ __('app.reserves') }}</th>
                                <th>{{ __('app.courses') }}</th>
                                <th>BL</th>
                                <th>TS</th>
                                <th>{{ __('app.reviews') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paysystems as $paysystem)
                                <tr>
                                    <td><a href="{!! $paysystem->site_url !!}" target="_blank">{!! $paysystem->name !!}</a></td>
                                    <td class="text-{!! ($paysystem->status == 'Active')?'high':'low' !!}">{!! $paysystem->status !!}</td>
                                    <td>{!! isset($paysystem->reserves_sum)?'$'.$paysystem->reserves_sum:'-' !!}</td>
                                    <td>{!! isset($paysystem->cources_count)?:'-' !!}</td>
                                    <td>{!! isset($paysystem->bl)?$paysystem->bl:'-' !!}</td>
                                    <td>{!! isset($paysystem->ts)?$paysystem->ts:'-' !!}</td>
                                    <td><a href="{!! $paysystem->site_url !!}" target="_blank"><span class="text-low">{!! $paysystem->negative_views_count !!}</span>/<span class="text-high">{!! $paysystem->positive_views_count !!}</span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $paysystems->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-4 filter-block">
                <div class="buy-filter-fix"></div>
                <h2>{{ __('app.exchange') }}</h2>
                <form class="main-page-block black buy-filter" action="{{ route('buy_sell.do_exchange') }}">
                    <div class="row">
                        <div class="col-xs-6">
                            <p>{{ __('app.give') }}</p>
                            <div class="select-box btn-block input-sm">
                                <select name="from_currency">
                                    <option value="EUR" {{ (isset($doExchange['from_currency']) && strtoupper($doExchange['from_currency']) == 'EUR') ? 'selected' : '' }}>EUR</option>
                                    <option value="USD" {{ (isset($doExchange['from_currency']) && strtoupper($doExchange['from_currency']) == 'USD') ? 'selected' : '' }}>USD</option>
                                    <option value="RUB" {{ (isset($doExchange['from_currency']) && strtoupper($doExchange['from_currency']) == 'RUB') ? 'selected' : '' }}>RUB</option>
                                </select>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <p>{{ __('app.get') }}</p>
                            <div class="select-box btn-block input-sm">
                                <select name="to_currency">
                                    <option value="EUR" {{ (isset($doExchange['to_currency']) && strtoupper($doExchange['to_currency']) == 'EUR') ? 'selected' : '' }}>EUR</option>
                                    <option value="USD" {{ (isset($doExchange['to_currency']) && strtoupper($doExchange['to_currency']) == 'USD') ? 'selected' : '' }}>USD</option>
                                    <option value="RUB" {{ (isset($doExchange['to_currency']) && strtoupper($doExchange['to_currency']) == 'RUB') ? 'selected' : '' }}>RUB</option>
                                </select>
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn btn-block btn-default">Показать</button>
                        </div>
                    </div>
                </form>
                <h2>{{ __('app.courses') }}</h2>
                <form class="main-page-block">
                    <ul class="buy-popular">
                            <!-- TradingView Widget BEGIN -->
                            <div class="tradingview-widget-container">
                                <div class="tradingview-widget-container__widget"></div>
                                <div class="tradingview-widget-copyright"><a href="https://ru.tradingview.com/symbols/FX_IDC-{{ (isset($doExchange['from_currency']) && isset($doExchange['to_currency'])) ? strtoupper($doExchange['from_currency']).strtoupper($doExchange['to_currency']) : 'EURUSD'}}/" rel="noopener" target="_blank">
                                <script type="text/javascript" src="https://s3.tradingview.com/external-embedding/embed-widget-single-quote.js" async>
                                    {
                                        "locale": "en_EN",
                                        "symbol": "FX_IDC:{{ (isset($doExchange['from_currency']) && isset($doExchange['to_currency'])) ? strtoupper($doExchange['from_currency']).strtoupper($doExchange['to_currency']) : 'EURUSD'}}",
                                        "width": "100%"
                                    }
                                </script>
                            </div>
                            <!-- TradingView Widget END -->
                    </ul>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <a href="{{ route('categories') }}" class="btn btn-default btn-block">{{ __('app.more_news') }}</a>
            </div>
        </div>
    </div>

    <script>
        function main_news_height() {
            var highestBox = 0;
            $('.main-news-list').each(function () {
                $(this).height('auto');
                if ($(this).height() > highestBox) {
                    highestBox = $(this).height();
                }
            }).height(highestBox);
        }
        $(document).ready(function () {
            main_news_height();
            $('.btn-filter-show').click(function () {
                $('.filter-wrapper').toggleClass('active');
            });
            $('.buy-filter .btn, .buy-popular button').click(function () {
                $('.filter-wrapper').removeClass('active');
            });
        });
        $(window).resize(function () {
            main_news_height();
            $('.filter-wrapper').removeClass('active');
        });
    </script>

@stop