@extends("layout")
@section("body")
    <div class="container articles">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{{ __('app.homepage') }}</a></li>
            <li class="active">{{ __('app.library') }}</li>
        </ol>
        <h2>{{ __('app.hot_topics') }}</h2>
        <div class="row">
            @foreach($categories as $category)
                <div class="col-sm-6 art-col">
                    <div class="main-page-block main-news-list">
                        <a style="position: relative; right: 0; bottom: 0;" href="/categories/{{ $category->slug }}"><h5 style="background: #3a505f url('{{asset('storage').'/'.$category->preview_img}}') no-repeat center;">{!! $category->name !!}</h5></a>
                        <div class="row">
                            <div class="col-xs-6 art-inner-col">
                                <ul>
                                    @foreach($category->posts as $key => $post)
                                        @if(ceil(count($category->posts)/2) == $key)
                                </ul>
                            </div>
                            <div class="col-xs-6 art-inner-col">
                                <ul>
                                    @endif
                                        <li>
                                            <article>
                                               <a href="/categories/{{ $category->slug }}/{{ $post->slug }}">
                                                    <div class="img">
                                                        <img src="{{ asset('storage').'/'.$post->image }}" />
                                                    </div>
                                                    <div class="text">
                                                        <p>{{ $post->title }}</p>
                                                    </div>
                                                </a>
                                            </article>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <ul class="pagination pagination-sm">
                            <li class="disabled"><a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                            <li class="active"><span>1</span></li>
                            <li><a href="https://cryptanio.com/categories/kotirovki?page=2">2</a></li>
                            <li><a href="https://cryptanio.com/categories/kotirovki?page=2" rel="next"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    </div>
@stop