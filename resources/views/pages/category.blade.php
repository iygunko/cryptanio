@extends("layout")
@section("body")

    <div class="container news">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{{ __('app.homepage') }}</a></li>
            <li><a href="/categories">{{ __('app.library') }}</a></li>
            <li class="active">{!! $category->name !!}</li>
        </ol>
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <h2>{!! $category->name !!}</h2>
                <div class="main-page-block">
                    @if(isset($category->posts[0]))
                    <article class="news-one-main">
                        <div class="img">
                            <a href="/categories/{{ $category->slug }}/{{ $category->posts[0]->slug }}"><img src="{{ asset('storage').'/'.$category->posts[0]->image }}" alt="main-news-one"></a>
                        </div>
                        <div class="text">
                            <h3><a href="/categories/{{ $category->slug }}/{{ $category->posts[0]->slug }}">{!! $category->posts[0]->title !!}</a></h3>
                            <p>{!! $category->posts[0]->excerpt !!}
                            </p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="time-wrapper">
                            <i class="fa fa-clock-o" aria-hidden="true"></i>
                            <time>{{ getTimeAgo( $category->posts[0]->created_at) }}</time>
                        </div>
                        <ul>
                            <li><i class="fa fa-eye" aria-hidden="true"></i>{{ $category->posts[0]->view_count }}</li>
                        </ul>
                    </article>
                    @endif
                    @for($i = 1 ; $i < count($categoryPosts); $i++)
                        <article class="news-one">
                            <div class="img">
                                <a href="/categories/{{ $category->slug }}/{{ $categoryPosts[$i]->slug }}"><img src="{{ asset('storage').'/'.$categoryPosts[$i]->image }}" alt="main-news-1"></a>
                            </div>
                            <div class="text">
                                <h3><a href="/categories/{{ $category->slug }}/{{ $categoryPosts[$i]->slug }}">{!! $categoryPosts[$i]->title !!}</a></h3>
                                <p>{!! $categoryPosts[$i]->excerpt !!}</p>
                                <div class="time-wrapper">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                    <time>{{ getTimeAgo($categoryPosts[$i]->created_at) }}</time>
                                </div>
                                <div class="pull-right">
                                    <ul>
                                        <li><i class="fa fa-eye" aria-hidden="true"></i>{{ $categoryPosts[$i]->view_count }}</li>
                                    </ul>
                                </div>
                            </div>
                        </article>
                    @endfor
                </div>
                {{ $categoryPosts->links() }}
            </div>
            <div class="col-md-5 col-lg-4">
                <h2>{{ __('app.other_news') }}</h2>
                <div class="main-page-block other-news">
                    @foreach($otherPosts as $otherPost)
                        <article>
                            <a href="/categories/{{ $otherPost->category->slug }}/{{ $otherPost->slug }}">
                                <div class="img">
                                    <img src="{{ asset('storage').'/'.$otherPost->image }}" alt="main-news-1">
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time>{{ getTimeAgo($otherPost->created_at) }}</time>
                                    </div>
                                    <div><i class="fa fa-eye" aria-hidden="true"></i>{{ $otherPost->view_count }}</div>
                                    <p>{!! cutIfLonger($otherPost->title, 55) !!}</p>
                                </div>
                            </a>
                        </article>
                    @endforeach

                </div>
                <h2>{{ __('app.ico_calendar') }}</h2>
                <div class="main-page-block main-ico">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab">{{ __('app.upcoming') }}</a></li>
                        <li><a href="#tab-2" data-toggle="tab">{{ __('app.current') }}</a></li>
                        <li><a href="#tab-3" data-toggle="tab">{{ __('app.past_ico') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="tab-1">
                            @foreach($sidebarIcos['futureIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>Start
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>End
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            @foreach($sidebarIcos['currentIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>Start
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>End
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-3">
                            @foreach($sidebarIcos['pastIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>Start
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>End
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <a href="#" class="btn btn-default">{{ __('app.watch_calendar') }}<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>
@stop