@extends("layout")
@section("body")
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{!! __('app.homepage') !!}</a></li>
            <li class="active">{!! $cryptocoin->name !!}</li>
        </ol>
        <div class="bitcoin-top">
            <div class="btc-top-main">
                <img src="{{asset('storage').'/'.$cryptocoin->icon}}">
                <h5>{!! $cryptocoin->name !!} ( {!! $cryptocoin->code !!} )</h5>
                <div>
                    <h6>$ {!! $cryptocoin->price !!}</h6>
                    <div class="select-box">
                        <select>
                            <option selected="">USD</option>
                            <option>EUR</option>
                            <option>RUB</option>
                        </select>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div>
                    <p class="text-high"><i class="fa fa-angle-{{ ((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price) > 0) ? 'up' : 'down' }}" aria-hidden="true"></i>{!! $cryptocoin->price-$cryptocoin->old_price !!} (20.27%)</p>
                </div>
            </div>
            <!------------------------------will be added later-------------------------------->
            <!--div class="btn-group">
                <a href="#" class="btn btn-default btn-sm">Add Prortfolio</a>
                <a href="#" class="btn btn-default btn-sm">Folowing</a>
            </div-->
            <ul>
                <li>
                    <ul>
                        <li>{!! __('app.trade_volume') !!}( 24h )<span>{{ $cryptocoin->trading_volume }}B</span></li>
                        <li>{!! __('app.market_cap') !!}<span>{{ $cryptocoin->market_cap }}B</span></li>
                    </ul>
                </li>
                <li>
                    <ul>
                        <li>Low24h<span>$ {{ $cryptocoin->low_price_24h }}</span></li>
                        <li>High24h<span>$ {{ $cryptocoin->high_price_24h }}</span></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-8">
                <h2>{!! __('app.short_info') !!}</h2>
                <div class="btc-info">{!! $cryptocoin->description !!}</div>
                <div class="btc-graph">
                    <!-- TradingView Widget BEGIN -->


                    <div id="tv-medium-widget-1"></div>



                    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                    <script type="text/javascript">
                        new TradingView.MediumWidget({
                            "container_id": "tv-medium-widget-1",
                            "symbols": [
                                [
                                    "{!! $cryptocoin->code !!} / USD",
                                    "COINBASE:{!! $cryptocoin->code !!}USD|3m"
                                ]
                            ],
                            "greyText": "{{ __('app.quotations_are_provided') }}",
                            "gridLineColor": "rgb(233, 233, 234)",
                            "fontColor": "rgb(218, 221, 224)",
                            "underLineColor": "rgba(60, 188, 152, 0.05)",
                            "trendLineColor": "rgb(60, 188, 152)",
                            "width": "100%",
                            "height": "100%",
                            "locale": "ru"
                        });
                    </script>
                    <!-- TradingView Widget END -->
                </div>
            </div>
            <div class="col-md-4">
                <h2>{!! __('app.course') !!} {!! $cryptocoin->name !!}</h2>
                <div class="main-page-block main-ico btc-side-course">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab">{!! __('app.upcoming') !!}</a></li>
                        <li><a href="#tab-2" data-toggle="tab">{!! __('app.current') !!}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="tab-1">
                            <ul class="btc-side-list">
                                <li>{!! __('app.burses') !!}</li>
                                <li>{!! __('app.burses') !!}</li>
                                <li>{!! __('app.burses') !!}</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <hr>
                            <ul class="btc-side-list">
                                <li>Средний</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>Минимальный</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>Максимальный</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            <ul class="btc-side-list">
                                <li>Биржа</li>
                                <li>Покупка</li>
                                <li>Продажа</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>name broker</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <hr>
                            <ul class="btc-side-list">
                                <li>Средний</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>Минимальный</li>
                                <li><i class="fa fa-angle-up text-high" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,563</li>
                            </ul>
                            <ul class="btc-side-list">
                                <li>Максимальный</li>
                                <li><i class="fa fa-angle-up text-transparent" aria-hidden="true"></i>11,253</li>
                                <li><i class="fa fa-angle-up text-low" aria-hidden="true"></i>11,563</li>
                            </ul>
                        </div>
                    </div>
                    <a href="#" class="btn btn-default">Смотреть все<i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <h2>{!! __('app.usefull_info') !!}Полезная информация</h2>
        <div class="row btc">
            @foreach($relatedCategories as $relatedCategory)
            <div class="col-md-6">
                <div class="main-page-block main-news-list main-news-list-bitcoin">
                    <h5>{!! $relatedCategory->name !!}</h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul>
                                @foreach($relatedCategory->posts as $key => $post)
                                    @if(ceil(count($relatedCategory->posts)/2) == $key)
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                    @endif
                                <li><a href="{{ URL::route('article', ['categorySlug' => $post->category->slug, 'postSlug' => $post->slug]) }}">{{ $post->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>

    <script>
        function iframe_styles() {
            var that = $('.btc-graph').find('iframe');
            $(that).attr('style', 'width:'+ ($('.btc-graph').width() + 2) + 'px');
        }
        $(document).ready(function () {
            iframe_styles();
        });
        $(window).resize(function () {
            iframe_styles();
        });
    </script>
@stop