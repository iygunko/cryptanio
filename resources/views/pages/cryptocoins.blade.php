@extends("layout")
@section("body")

    <div class="container crypto-list">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{!! __('app.homepage') !!}</a></li>
            <li class="active">{!! __('app.cryptocoins') !!}</li>
        </ol>
        <div class="crypto-list-select">
            <div class="cl-select active">
                <span>All<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                <div class="cl-select-inner">
                    <a href="#" class="active">Top 100</a>
                    <a href="#">Full list</a>
                </div>
            </div>
            <div class="cl-select">
                <span>Coins<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                <div class="cl-select-inner">
                    <a href="#">Top 100</a>
                    <a href="#">Full list</a>
                    <a href="#">Market Cap by Circulating Supply</a>
                    <a href="#">Market Cap by Total Supply</a>
                    <a href="#">Filter Non-Mineable</a>
                </div>
            </div>
            <div class="cl-select">
                <span>Tokens<i class="fa fa-angle-down" aria-hidden="true"></i></span>
                <div class="cl-select-inner">
                    <a href="#">Top 100</a>
                    <a href="#">Full list</a>
                    <a href="#">Market Cap by Circulating Supply</a>
                    <a href="#">Market Cap by Total Supply</a>
                </div>
            </div>
            <div class="cl-select">
                <span><strong>RUB</strong><i class="fa fa-angle-down" aria-hidden="true"></i></span>
                <div class="cl-select-inner">
                    <a href="#">RUB</a>
                    <a href="#">USD</a>
                    <a href="#">BTC</a>
                    <a href="#">ETH</a>
                    <a href="#">XRP</a>
                    <a href="#">BCH</a>
                    <a href="#">LTC</a>
                </div>
            </div>
        </div>


        <div class="main-page-table" >
            <div class="main-page-block main-page-table-wrap">
                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!! __('app.name') !!}</th>
                        <th>{!! __('app.market_cap') !!}</th>
                        <th>{!! __('app.price') !!}</th>
                        <th>{!! __('app.volume') !!} (24h)</th>
                        <th>{!! __('app.circulating_supply') !!}</th>
                        <th>{!! __('app.change') !!} (24h)</th>
                        <th>{!! __('app.price_graph') !!} (7d)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($cryptocoins as $key => $cryptocoin)
                    <tr>
                        <td>{{ $key+1 }}</td>
                        <td class="table-link">
                            <a href="{{ route('cryptocoin', $cryptocoin->slug) }}">
                                <img src="{{asset('storage').'/'.$cryptocoin->icon}}">{!! $cryptocoin->name !!}
                            </a>
                        </td>
                        <td>{!! $cryptocoin->market_cap !!}</td>
                        <td>{!! $cryptocoin->price !!}</td>
                        <td>{!! $cryptocoin->trading_volume !!}</td>
                        <td>{!! $cryptocoin->circulating_supply !!}</td>
                        <td class="text-{{ ((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price) > 0) ? 'high' : 'low' }}">{{ number_format((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price), 3, ',', ' ') }}%</td>
                        <td><img src="{{asset('img/main-table-graph.png')}}"></td>
                    </tr>

                    <tr class="main-page-table-info-row">
                        <td colspan="8">
                            <div class="main-table-info">
                                <div class="left">
                                    <div class="main-info-main">
                                        <img src="{{asset('storage').'/'.$cryptocoin->icon}}">
                                        <h5><a href="{{ route('cryptocoin', $cryptocoin->slug) }}">{!! $cryptocoin->name !!} ( {!! $cryptocoin->code !!} )</a></h5>
                                        <p>{!! $cryptocoin->price !!}</p>
                                        <span class="text-{{((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price) > 0) ? 'high' : 'low'}}"><i class="fa fa-angle-up" aria-hidden="true"></i>{{$cryptocoin->price-$cryptocoin->old_price}} ({{ number_format((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price), 3, ',', ' ')}}%)</span>
                                    </div>
                                    <div class="main-burse">
                                        <h5>{!! __('app.burses') !!}</h5>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul>
                                            @foreach($exchangers as $key => $exchanger)

                                                @if(count($exchangers)/2 < $key)
                                                    <li>
                                                        <a target="_blank" href="{!! $exchanger->site_url !!}"><img src="{{asset('img/main-burse-logo.png')}}">{!! $exchanger->name !!}</a>
                                                    </li>

                                                @else
                                                @if (count($exchangers)/2 == $key)
                                                </ul>
                                            </div>
                                            <div class="col-xs-6">
                                                <ul>
                                                    @else
                                                        <li>
                                                            <a target="_blank" href="{!! $exchanger->site_url !!}"><img src="{{asset('img/main-burse-logo.png')}}">{!! $exchanger->name !!}</a>
                                                        </li>
                                                    @endif
                                                @endif
                                            @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="right">

                                    <!-- TradingView Widget BEGIN -->

                                    <div id="tv-medium-widget-{{$key+1}}"></div>

                                    <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                    <script type="text/javascript">
                                        new TradingView.MediumWidget({
                                            "container_id": "tv-medium-widget-{{$key+1}}",
                                            "symbols": [
                                                [
                                                    "LTC / USD",
                                                    "COINBASE:{{$cryptocoin->code}}USD|3m"
                                                ]
                                            ],
                                            "greyText": "Котировки предоставлены",
                                            "gridLineColor": "rgb(233, 233, 234)",
                                            "fontColor": "rgb(218, 221, 224)",
                                            "underLineColor": "rgba(60, 188, 152, 0.05)",
                                            "trendLineColor": "rgb(60, 188, 152)",
                                            "width": "100%",
                                            "height": "100%",
                                            "locale": "ru"
                                        });
                                    </script>
                                    <!-- TradingView Widget END -->

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="crypto-list-pagination">
            <p><sub>*</sub>{!! __('app.not_mineable') !!}</p>
            @if(method_exists($cryptocoins, 'links'))
            <a href="{{route('cryptocoins')}}?all=1"><button type="button" class="btn btn-bordered">View All</button></a>
            {!! $cryptocoins->links('vendor.pagination.simple-coins') !!}
            @endif
        </div>
        <ul class="crypto-list-info">
            <li>
                <h5>{!! __('app.total_market_cap') !!}</h5>
                <h6>$ 469,121,157,893</h6>
            </li>
            <li>
                <h5>24h {!! __('app.total_trade_volume') !!}</h5>
                <h6>$ 48,121,157,893</h6>
            </li>
            <li>
                <h5>{!! __('app.total_coins_number') !!}</h5>
                <h6>$ 48,121,157,893</h6>
            </li>
        </ul>
    </div>

    <script>
        $('.cl-select span').click(function () {
            if($(this).siblings('.cl-select-inner').hasClass('active')) {
                $('.cl-select-inner').removeClass('active').stop().slideUp(300);
            }
            else {
                $('.cl-select-inner').removeClass('active').stop().slideUp(300);
                $(this).siblings('.cl-select-inner').addClass('active').stop().slideDown(300);
            }
        });

        $('.cl-select-inner').not(':last').children('a').click(function () {
            $('.cl-select-inner').stop().slideUp(300);
            $('.cl-select-inner a, .cl-select-inner, .cl-select').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.cl-select').addClass('active');
        });

        $('.cl-select-inner:last a').click(function () {
            $('.cl-select-inner').removeClass('active').stop().slideUp(300);
            $(this).closest('.cl-select').children('span').children('strong').text($(this).text());
        });

        $('body').on('click', function (e) {
            if(!$(e.target).parentsUntil('cl-select').parent().hasClass('cl-select') && !$(e.target).parent().hasClass('cl-select')){
                $('.cl-select-inner').removeClass('active').stop().slideUp(300);
            }
        });

        function iframeHeight() {
            $('.main-page-table-info-row .right > div').each(function () {
                var that = $(this);
                $(that).height(0);
                $(that).find('iframe').height(0);
                $(that).height($(that).closest('.main-page-table-info-row').find('.left').height());
                $(that).find('iframe').attr('style', 'padding: 0').height($(that).closest('.main-page-table-info-row').height() + 41);
                $(that).closest('.main-table-info').css('padding-bottom','3px');
            });
        }
        $(document).ready(function () {
            $('.main-page-table-wrap tbody tr').not('.main-page-table-info-row').click(function () {
                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                    $('.main-page-table-info-row').stop().hide(500);
                }
                else{
                    $('.main-page-table-wrap tbody tr').removeClass('active');
                    $(this).addClass('active');
                    $('.main-page-table-info-row').stop().hide(500);
                    $(this).next('.main-page-table-info-row').stop().show(500);
                    iframeHeight();
                }
            });
        });
        $(window).resize(function () {
            iframeHeight();
        });
    </script>
@stop