@extends("layout")
@section("body")
    <script src='https://www.google.com/recaptcha/api.js'></script><!--Secret: 6LeUmXMUAAAAAFFIONd3rQ6ZzVO7cxIWLsaArjbi-->

    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{!! __('app.homepage') !!}</a></li>
            <li class="active">Контакты</li>
        </ol>
        <div class="row">
            <div class="col-sm-5 col-lg-6">
                <div class="contacts">
                    <h2>Контакты</h2>
                    <h4>Позвоните нам</h4>
                    <ul>
                        <li>
                            <a href="tel:+75864521214">+7 (586) 452-12-14</a>
                        </li>
                        <li>
                            <a href="tel:+75864521214">+7 (586) 452-12-14</a>
                        </li>
                        <li>
                            <a href="tel:+75864521214">+7 (586) 452-12-14</a>
                        </li>
                    </ul>
                    <h4>Напишите нам на почту</h4>
                    <ul>
                        <li>
                            <a href="mailto:info@cryptanio.com">info@cryptanio.com</a>
                        </li>
                    </ul>
                    <h4>Напишите нам в скайп</h4>
                    <ul>
                        <li>
                            <a href="skype:cryptanio.com?chat">cryptanio.com</a>
                        </li>
                    </ul>
                    <h4>Мы в соцсетях</h4>
                    <div class="social">
                        <a href="#" target="_blank">
                            <img src="{{asset('img/foot-vk-2.png')}}" alt="vkontakte">
                        </a>
                        <a href="#" target="_blank">
                            <img src="{{asset('img/foot-fb-2.png')}}" alt="facebook">
                        </a>
                        <a href="#" target="_blank">
                            <img src="{{asset('img/foot-tw-2.png')}}" alt="twitter">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-7 col-lg-6">
                <div class="feedback-wrap">
                    <h2>Обратная связь</h2>
                    <form class="feedback">
                        <h4>Оставте ваше сообщение и мы ответим в ближайшее время</h4>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>Имя</p>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <p>E-mail</p>
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <p>Тема</p>
                            <input type="text">
                        </div>
                        <div class="form-group">
                            <p>Сообщение</p>
                            <textarea></textarea>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6LeUmXMUAAAAAMSRnESJLVs7L0RgO35a_skAY0xd"></div>
                        <button type="button" class="btn btn-default btn-lg">Отправить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop