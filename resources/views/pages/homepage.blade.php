@extends("layout")
@section("body")
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <h2>{{ __('app.important_news') }}</h2>
                <div class="main-page-block">
                    @foreach($homeNews as $key => $homeNew)
                        @if($key == 0)
                            <article class="main-news-one">
                                <div class="img">
                                    <a href="/categories/{{$homeNew->category->slug}}/{{$homeNew->slug}}"><img
                                                src="{{ asset('storage').'/'.$homeNew->image }}"
                                                alt="{{$homeNew->title}}"></a>
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time class="timeago"
                                              datetime="{{ $homeNew->created_at }}">{{ $homeNew->created_at }}</time>
                                    </div>
                                    <div class="pull-right">
                                        <ul>
                                            <li><i class="fa fa-eye" aria-hidden="true"></i>{{$homeNew->view_count}}
                                            </li>
                                        </ul>
                                    </div>
                                    <h3>{!! cutIfLonger($homeNew->title, 55) !!}</h3>
                                    <p>{!! $homeNew->excerpt !!}</p>
                                    <a href="/categories/{{$homeNew->category->slug}}/{{$homeNew->slug}}"
                                       class="btn btn-default">{{ __('app.read_more') }}<i class="fa fa-angle-right"
                                                                                           aria-hidden="true"></i></a>
                                </div>
                            </article>
                        @elseif($key == 1)
                            <div class="row">
                                <article class="col-sm-6">
                                    <a class="main-news"
                                       href="/categories/{{$homeNew->category->slug}}/{{$homeNew->slug}}">
                                        <div class="img">
                                            <img src="{{ asset('storage').'/'.$homeNew->image }}" alt="main-news-1">
                                        </div>
                                        <div class="text">
                                            <div class="time-wrapper">
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                <time class="timeago" datetime="{{ $homeNew->created_at }}"></time>
                                            </div>
                                            <p>{!! cutIfLonger($homeNew->title, 55) !!}</p>
                                        </div>
                                    </a>
                                </article>
                                @else
                                    <article class="col-sm-6">
                                        <a class="main-news"
                                           href="/categories/{{$homeNew->category->slug}}/{{$homeNew->slug}}">
                                            <div class="img">
                                                <img src="{{ asset('storage').'/'.$homeNew->image }}"
                                                     alt="main-news-1">
                                            </div>
                                            <div class="text">
                                                <div class="time-wrapper">
                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                    <time class="timeago" datetime="{{ $homeNew->created_at }}"></time>
                                                </div>
                                                <p>{!! $homeNew->title !!}</p>
                                            </div>
                                        </a>
                                    </article>
                                @endif
                                @endforeach
                            </div>
                            <a href="/categories" class="btn btn-default">{{ __('app.more_news') }}<i
                                        class="fa fa-angle-right"
                                        aria-hidden="true"></i></a>
                </div>
            </div>


            <div class="col-md-5 col-lg-4">
                <h2>{{ __('app.ico_calendar') }}</h2>
                <div class="main-page-block main-ico">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab">{{ __('app.upcoming') }}</a></li>
                        <li><a href="#tab-2" data-toggle="tab">{{ __('app.current') }}</a></li>
                        <li><a href="#tab-3" data-toggle="tab">{{ __('app.past_ico') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="tab-1">
                            @foreach($sidebarIcos['futureIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            @foreach($sidebarIcos['currentIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-3">
                            @foreach($sidebarIcos['pastIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <a href="{{ route('icos') }}" class="btn btn-default">{{ __('app.watch_calendar') }}<i
                                class="fa fa-angle-right"
                                aria-hidden="true"></i></a>
                </div>
            </div>
        </div>

        <div class="main-page-table">
            <h2>{{ __('app.top_crypto') }}</h2>
            <div class="select-box">
                <select>
                    <option selected>USD</option>
                    <option>EUR</option>
                    <option>RUB</option>
                </select>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </div>
            <div class="main-page-block main-page-table-wrap">
                <table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{!! __('app.name') !!}</th>
                        <th>{!! __('app.market_cap') !!}</th>
                        <th>{!! __('app.price') !!}</th>
                        <th>{!! __('app.volume') !!} (24h)</th>
                        <th>{!! __('app.circulating_supply') !!}</th>
                        <th>{!! __('app.change') !!} (24h)</th>
                        <th>{!! __('app.price_graph') !!} (7d)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($top5Cryptocoins as $key => $cryptocoin)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td class="table-link">
                                <a href="{{ route('cryptocoin', $cryptocoin->slug) }}">
                                    <img src="{{asset('storage').'/'.$cryptocoin->icon}}">{!! $cryptocoin->name !!}
                                </a>
                            </td>
                            <td>{!! $cryptocoin->market_cap !!}</td>
                            <td>{!! $cryptocoin->price !!}</td>
                            <td>{!! $cryptocoin->trading_volume !!}</td>
                            <td>{!! $cryptocoin->circulating_supply !!}</td>
                            <td class="text-{{ ((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price) > 0) ? 'high' : 'low' }}">{{ number_format((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price), 3, ',', ' ') }}
                                %
                            </td>
                            <td><img src="{{asset('img/main-table-graph.png')}}"></td>
                        </tr>

                        <tr class="main-page-table-info-row">
                            <td colspan="8">
                                <div class="main-table-info">
                                    <div class="left">
                                        <div class="main-info-main">
                                            <img src="{{asset('storage').'/'.$cryptocoin->icon}}">
                                            <h5>
                                                <a href="{{ route('cryptocoin', $cryptocoin->slug) }}">{!! $cryptocoin->name !!}
                                                    ( {!! $cryptocoin->code !!} )</a></h5>
                                            <p>{!! $cryptocoin->price !!}</p>
                                            <span class="text-{{((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price) > 0) ? 'high' : 'low'}}"><i
                                                        class="fa fa-angle-up"
                                                        aria-hidden="true"></i>{{$cryptocoin->price-$cryptocoin->old_price}}
                                                ({{ number_format((100*($cryptocoin->price-$cryptocoin->old_price)/$cryptocoin->old_price), 3, ',', ' ')}}
                                                %)</span>
                                        </div>
                                        <div class="main-burse">
                                            <h5>{!! __('app.burses') !!}</h5>
                                            <div class="row">
                                                <div class="col-xs-6">
                                                    <ul>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-6">
                                                    <ul>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                        <li>
                                                            <a href="#"><img src="{{asset('img/main-burse-logo.png')}}">Coinbase</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="right">

                                        <!-- TradingView Widget BEGIN -->

                                        <div id="tv-medium-widget-{{$key+1}}"></div>

                                        <script type="text/javascript" src="https://s3.tradingview.com/tv.js"></script>
                                        <script type="text/javascript">
                                            new TradingView.MediumWidget({
                                                "container_id": "tv-medium-widget-{{$key+1}}",
                                                "symbols": [
                                                    [
                                                        "LTC / USD",
                                                        "COINBASE:{{$cryptocoin->code}}USD|3m"
                                                    ]
                                                ],
                                                "greyText": "Котировки предоставлены",
                                                "gridLineColor": "rgb(233, 233, 234)",
                                                "fontColor": "rgb(218, 221, 224)",
                                                "underLineColor": "rgba(60, 188, 152, 0.05)",
                                                "trendLineColor": "rgb(60, 188, 152)",
                                                "width": "100%",
                                                "height": "100%",
                                                "locale": "ru"
                                            });
                                        </script>
                                        <!-- TradingView Widget END -->

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <a href="{{route('cryptocoins')}}?all=1" class="btn btn-default">{{ __('app.watch_all') }}<i
                            class="fa fa-angle-right"
                            aria-hidden="true"></i></a>
            </div>
        </div>

        <div class="row">
            @foreach($homeCategories as $key => $homeCategory)
                <div class="col-md-6">
                    <div class="main-page-block main-news-list main-news-list-mining">
                        <a style="position: relative; right: 0; bottom: 0;"
                           href="/categories/{{ $homeCategory->slug }}"><h5>{!! $homeCategory->name !!}</h5></a>
                        <div class="row">
                            <div class="col-sm-6">
                                <ul>
                                    @foreach($homeCategory->posts as $key => $post)
                                        @if(ceil(count($homeCategory->posts)/2) == $key)
                                </ul>
                            </div>
                            <div class="col-sm-6">
                                <ul>
                                    @endif
                                    <li>
                                        <a href="/categories/{{ $homeCategory->slug }}/{{ $post->slug }}">{{ $post->title }}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <a href="{{ route('categories') }}" class="btn btn-default btn-block">{{ __('app.more_news') }}</a>
            </div>
        </div>
    </div>
@stop