@extends("layout")
@section("body")

    <div class="container ico-full-wrapper">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{!! __('app.homepage') !!}</a></li>
            <li><a href="{{ URL::route('icos') }}">ICOS</a></li>
            <li class="active">{!! $ico->name !!}</li>
        </ol>
        <div class="row">
            <div class="col-md-8">
                <h2>{!! strtoupper($ico->name) !!}</h2>
                <div class="main-page-block ico-full">
                    <div class="ico-info-main">
                        <div class="img">
                            <img src="{{ asset('storage').'/'.$ico->icon }}" alt="ico-apex-logo">
                        </div>
                        <div class="text">
                            <strong>{!! $ico->name !!}</strong>
                            <span>{!! __('app.token_price') !!}</span>
                            <p class="text-high">${{ $ico->price }}</p>
                        </div>
                    </div>
                    <!--div class="pull-right">
                        <button type="button" class="btn btn-default">
                            Следить
                        </button>
                    </div-->
                    <p>{!! $ico->description !!}</p>
                    <div class="ico-rate">
                        <div>
                            <p>
                                {!! __('app.rating') !!}:
                                <i class="fa fa-info-circle tooltip" aria-hidden="true">
                                    <span class="tooltiptext">{!! __('app.ico_rating_source') !!}</span>
                                </i>
                            </p>
                        </div>
                        <div>
                            <strong>{!! explode('|', $ico->rating)[0] !!}</strong>
                            {!! __('app.marketing') !!}
                        </div>
                        <div>
                            <strong>{!! explode('|', $ico->rating)[1] !!}</strong>
                            {!! __('app.product') !!}
                        </div>
                        <div>
                            <strong>{!! explode('|', $ico->rating)[2] !!}</strong>
                            {!! __('app.potential') !!}
                        </div>
                        <div>
                            <strong>{!! explode('|', $ico->rating)[3] !!}</strong>
                            {!! __('app.prediction') !!}
                        </div>
                    </div>
                    <div class="ico-progress">
                        <p>
                            {!! __('app.progress') !!}:
                            <i class="fa fa-info-circle tooltip" aria-hidden="true">
                                <span class="tooltiptext">{!! __('app.progress') !!}</span>
                            </i>
                        </p>
                        <div>
                            <span>{{ $ico->progress }} USD</span>
                            <span>{{ ($ico->progress/$ico->purpose)*100 }}%</span>
                            <span>{{ $ico->purpose }} USD</span>
                            <div style="width: {{ ($ico->progress/$ico->purpose)*100 }}%"></div>
                        </div>
                    </div>
                    <div class="ico-video">
                        <iframe width="560" height="315" src="{!! $ico->video_url !!}?rel=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        <div class="ico-video-overlay"></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>Pree-Sale</p>
                                <p>{!! $ico->pre_sale !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>iCO</p>
                                <p>{!! $ico->ico !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.to_start') !!}</p>
                                <div class="ico-count" data-count="50000"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.site') !!}</p>
                                <p><a href="{!! $ico->site_url !!}">{!! $ico->name !!}</a></p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.status') !!}</p>
                                <p>{!! $ico->status !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.category') !!}</p>
                                <p><a href="{{ route('category', $ico->category->slug) }}">{!! $ico->category->name !!}</a></p>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.platform') !!}</p>
                                <p>{!! $ico->platform !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.list_participations') !!}</p>
                                <p>{!! $ico->register_time !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.verification') !!}</p>
                                <p>{{ ($ico->verification)  ? 'Есть' : 'Нет'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.price_in_tokens') !!}</p>
                                @for($i = 0; $i < count(explode('|', $ico->token_prices)); $i++)
                                    <p><strong>{!! explode('|', $ico->token_prices)[$i] !!}</strong></p>
                                @endfor
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.participation_limits') !!}</p>
                                <p>{{ ($ico->participants_limit)  ? 'Есть' : 'Нет'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.payment_systems') !!}</p>
                                @foreach(json_decode($ico->payment_systems) as $key => $paysystem)
                                    @if($key != count(json_decode($ico->payment_systems))-1 )
                                        <span>{!! $paysystem !!}</span>,
                                    @else
                                        <span>{!! $paysystem !!}</span>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.discounts') !!}</p>
                                <p>{!! $ico->discounts !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.general_emission') !!}</p>
                                <p>{!! $ico->full_emission !!}</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-4">
                            <div class="ico-info-block">
                                <p>{!! __('app.tokens_distributing') !!}</p>
                                <p>{!! $ico->tokens_distribution_time !!}</p>
                            </div>
                        </div>
                        <div class="col-xs-8">
                            <a href="{!! $ico->buy_token_link !!}" class="ico-btn-buy">{!! __('app.buy_tokens') !!} {!! $ico->name !!}</a>
                        </div>
                    </div>
                    <div class="sharethis-inline-share-buttons"></div>
                    <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5a8c394ce3b02a00133b2cbf&product=inline-share-buttons"></script>
                </div>
            </div>
            <div class="col-md-4">
                <h2>Другие новости</h2>
                <div class="main-page-block other-news">
                    @foreach($newPosts as $newPost)
                        <article>
                            <a href="/categories/{!! $newPost->category->slug !!}/{!! $newPost->slug !!}">
                                <div class="img">
                                    <img src="{{asset('storage')."/".$newPost->image}}" alt="{!! $newPost->title !!}">
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time class="timeago" datetime="{{ $newPost->created_at }}"><i class="fa fa-clock-o" aria-hidden="true"></i></time>
                                    </div>
                                    <div><i class="fa fa-eye" aria-hidden="true"></i>{!! $newPost->view_count !!}</div>
                                    <p>{!! $newPost->title !!}</p>
                                </div>
                            </a>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <script src="{{asset('js/jquery.plugin.js')}}"></script>
    <script src="{{asset('js/jquery.countdown.js')}}"></script>
    <script src="{{asset('js/jquery.countdown-ru.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.ico-count').each(function () {
                $(this).countdown({
                    until: $(this).attr('data-count'),
                    format: 'DHMS'
                });
            });
        });
    </script>
@stop