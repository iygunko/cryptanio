@extends("layout")
@section("body")
    <div class="container">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{!! __('app.homepage') !!}</a></li>
            <li class="active">ICO</li>
        </ol>
        <div class="row">
            <div class="col-md-8 ico-container">
                <h2>ICO</h2>
                <p>{!! __('app.icos_text') !!}</p>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab-1" data-toggle="tab">{!! __('app.upcoming') !!}<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-2" data-toggle="tab">{!! __('app.current') !!}<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-3" data-toggle="tab">{!! __('app.past_ico') !!}<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="tab-1">
                        <table class="ico-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{!! __('app.name') !!}</th>
                                <th>{!! __('app.token_price') !!}</th>
                                <th>{!! __('app.to_start') !!}</th>
                                <th>{!! __('app.status') !!}</th>
                                <th>{!! __('app.purpose') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="space">
                                <td colspan="6"></td>
                            </tr>
                            @foreach($futureIcos as $key => $futureIco)
                            <tr class="item">
                                <td><strong>{{ $key+1 }}</strong></td>
                                <td><a href="{{route('ico', $futureIco->slug)}}"><img src="{{ asset('storage')."/".$futureIco->icon }}">{!! $futureIco->name !!}</a></td>
                                <td>{!! $futureIco->price !!}</td>
                                <td><div class="ico-count" data-count="{{ strtotime($futureIco->start_time) - time()  }}"></div></td>
                                <td>{!! $futureIco->status !!}</td>
                                <td><strong>${!! $futureIco->purpose !!}<span>{{ ($futureIco->progress/$futureIco->purpose)*100 }}%</span></strong></td>
                            </tr>
                            <tr class="ico-progress">
                                <td colspan="6">
                                    <div><div style="width: {{ ($futureIco->progress/$futureIco->purpose)*100 }}%;"></div></div>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>

                        </table>
                        {{ $futureIcos->links() }}
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <table class="ico-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{!! __('app.name') !!}</th>
                                <th>{!! __('app.token_price') !!}</th>
                                <th>{!! __('app.to_start') !!}</th>
                                <th>{!! __('app.status') !!}</th>
                                <th>{!! __('app.purpose') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="space">
                                <td colspan="6"></td>
                            </tr>
                            @foreach($currentIcos as $key => $currentIco)
                                <tr class="item">
                                    <td><strong>{{ $key+1 }}</strong></td>
                                    <td><a href="{{route('ico', $currentIco->slug)}}"><img src="{{ asset('storage')."/".$currentIco->icon }}"></a>{!! $currentIco->name !!}</td>
                                    <td>{!! $currentIco->price !!}</td>
                                    <td><div class="ico-count" data-count="{{ strtotime($currentIco->start_time) - time() }}"></div></td>
                                    <td>{!! $currentIco->status !!}</td>
                                    <td><strong>${!! $currentIco->purpose !!}<span>{{ ($currentIco->progress/$currentIco->purpose)*100 }}%</span></strong></td>
                                </tr>
                                <tr class="ico-progress">
                                    <td colspan="6">
                                        <div><div style="width: {{ ($currentIco->progress/$currentIco->purpose)*100 }}%;"></div></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $currentIcos->links() }}
                    </div>
                    <div class="tab-pane fade" id="tab-3">
                        <table class="ico-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{!! __('app.name') !!}</th>
                                <th>{!! __('app.token_price') !!}</th>
                                <th>{!! __('app.to_start') !!}</th>
                                <th>{!! __('app.status') !!}</th>
                                <th>{!! __('app.purpose') !!}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="space">
                                <td colspan="6"></td>
                            </tr>
                            @foreach($pastIcos as $key => $pastIco)
                                <tr class="item">
                                    <td><strong>{{ $key+1 }}</strong></td>
                                    <td><a href="{{route('ico', $pastIco->slug)}}"><img src="{{ asset('storage')."/".$pastIco->icon }}"></a>{!! $pastIco->name !!}</td>
                                    <td>{!! $pastIco->price !!}</td>
                                    <td><div class="ico-count" data-count="{{ strtotime($pastIco->start_time) - time() }}"></div></td>
                                    <td>{!! $pastIco->status !!}</td>
                                    <td><strong>${!! $pastIco->purpose !!}<span>{{ ($pastIco->progress/$pastIco->purpose)*100 }}%</span></strong></td>
                                </tr>
                                <tr class="ico-progress">
                                    <td colspan="6">
                                        <div><div style="width: {{ ($pastIco->progress/$pastIco->purpose)*100 }}%;"></div></div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $pastIcos->links() }}
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <h2>{!! __('app.other_news') !!}</h2>
                <div class="main-page-block other-news">
                    @foreach($newPosts as $newPost)
                        <article>
                            <a href="/categories/{!! $newPost->category->slug !!}/{!! $newPost->slug !!}">
                                <div class="img">
                                    <img src="{{asset('storage')."/".$newPost->image}}" alt="{!! $newPost->title !!}">
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time class="timeago" datetime="{{ $newPost->created_at }}"><i class="fa fa-clock-o" aria-hidden="true"></i></time>
                                    </div>
                                    <div><i class="fa fa-eye" aria-hidden="true"></i>{!! $newPost->view_count !!}</div>
                                    <p>{!! $newPost->title !!}</p>
                                </div>
                            </a>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('js/jquery.plugin.js')}}"></script>
    <script src="{{asset('js/jquery.countdown.js')}}"></script>
    <script src="{{asset('js/jquery.countdown-ru.js')}}"></script>
    <!--script type="text/javascript" src="js/jquery.countdown-ua.js"></script-->
    <script>
        $(document).ready(function () {
            $('.ico-count').each(function () {
                $(this).countdown({
                    until: $(this).attr('data-count'),
                    format: 'DHMS'
                });
            });
        });
    </script>
@stop