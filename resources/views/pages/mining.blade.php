@extends("layout")
@section("body")
    <div class="container mine-wrap">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{{ __('app.homepage') }}</a></li>
            <li><a href="{{ URL::route('categories') }}">{{ __('app.library') }}</a></li>
            <li class="active">{!! $miningCategory->name !!}</li>
        </ol>
        <div class="row">
            <div class="col-md-8">
                <h2>{{ __('app.mining_calculator') }}</h2>
                <ul class="nav nav-tabs calculator-nav">
                    <li class="active"><a href="#tab-1" data-coin-code='BTC' data-toggle="tab">BTC<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-2" data-coin-code='ETH' data-toggle="tab">ETH<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-3" data-coin-code='ETC' data-toggle="tab">ETС<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-4" data-coin-code='XMR' data-toggle="tab">XMR<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-5" data-coin-code='ZEC' data-toggle="tab">ZEC<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-6" data-coin-code='PASC' data-toggle="tab">PASC<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                    <li><a href="#tab-7" data-coin-code='LTC' data-toggle="tab">LTC<i class="fa fa-angle-down" aria-hidden="true"></i></a></li>
                </ul>
                <input id ="selecetedCoinTab" type="hidden" value="BTC">
                <div class="tab-content">
                    <div class="tab-pane active fade in" id="tab-1">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>BITCOIN ( BTC )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 BTC = $ <span id="spanPriceForBTC">8745</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formBTC">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceBTC" type="text" value="7300">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueBTC" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureBTC'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionBTC" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostBTC" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeBTC" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i  $ 726.7k

День
Прибыли в день

$ 24.2k
Добыто/день

3.1307
Стоимость энергии/День

$ 2.9k
Неделя
Прибыли в неделю

$ 169.6k
Добыто/неделя

21.9149
Стоимость энергии/Неделя

$ 20.2k
Месяц
Прибыли в месяц

$ 726.7k
Добыто/месяц

93.9211
Стоимость энергии/Месяц

$ 86.4k
Год
Прибыли в год

$ 8841.8k
Добыто/год

1142.7070
Стоимость энергии/Год

$ 1051.2k
class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayBTC">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthBTC">91090</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayBTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayBTC" class="text-high">BTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayBTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekBTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekBTC" class="text-high">BTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekBTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthBTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthBTC" class="text-high">BTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthBTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearBTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearBTC" class="text-high">BTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearBTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_btc') !!}</div>
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Ethereum ( ETH )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 ETH = $ <span id="spanPriceForETH">593</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formETH">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceETH" type="text" value="451">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueETH" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureETH'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionETH" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostETH" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeETH" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayETH">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthETH">9109</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayETH" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayETH" class="text-high">ETH 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayETH" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekETH" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekETH" class="text-high">ETH 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekETH" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthETH" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthETH" class="text-high">ETH 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthETH" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearETH" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearETH" class="text-high">ETH 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearETH" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_eth') !!}</div>
                    </div>

                    <div class="tab-pane fade" id="tab-3">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Etherium ( ETC )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 ETC = $ <span id="spanPriceForETC">18</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formETC">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceETC" type="text" value="17">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueETC" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureETC'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionETC" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostETC" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeETC" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayETC">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthETC">91000</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayETC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayETC" class="text-high">ETC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayETC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekETC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekETC" class="text-high">ETC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekETC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthETC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthETC" class="text-high">ETC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthETC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearETC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearETC" class="text-high">ETC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearETC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_etc') !!}</div>
                    </div>
                    <div class="tab-pane fade" id="tab-4">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Monero ( XMR )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 XMR = $ <span id="spanPriceForXMR">247</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formXMR">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceXMR" type="text" value="128">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueXMR" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureXMR'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionXMR" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostXMR" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeXMR" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayXMR">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthXMR">91090</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayXMR" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayXMR" class="text-high">XMR 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayXMR" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekXMR" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekXMR" class="text-high">XMR 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekXMR" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthXMR" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthXMR" class="text-high">XMRC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthXMR" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearXMR" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearXMR" class="text-high">XMR 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearXMR" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_xmr') !!}</div>
                    </div>
                    <div class="tab-pane fade" id="tab-5">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Zcash ( ZEC )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 ZEC = $ <span id="spanPriceForZEC">261</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formZEC">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceZEC" type="text" value="194">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueZEC" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureZEC'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionZEC" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostZEC" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeZEC" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayZEC">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthZEC">91030</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayZEC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayZEC" class="text-high">ZEC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayZEC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekZEC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekZEC" class="text-high">ZEC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekZEC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthZEC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthZEC" class="text-high">ZEC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthZEC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearZEC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearZEC" class="text-high">ZEC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearZEC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_zec') !!}</div>
                    </div>
                    <div class="tab-pane fade" id="tab-6">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Pascal Coin ( PASC )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 PASC = $ <span id="spanPriceForPASC">0.989434</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formPASC">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPricePASC" type="text" value="0.68">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValuePASC" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasurePASC'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionPASC" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostPASC" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeePASC" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4>{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayPASC">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthPASC">91090</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayPASC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayPASC" class="text-high">PASC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayPASC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekPASC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekPASC" class="text-high">PASC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekPASC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthPASC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthPASC" class="text-high">PASC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthPASC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearPASC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearPASC" class="text-high">PASC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearPASC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_pasc') !!}</div>
                    </div>
                    <div class="tab-pane fade" id="tab-7">
                        <div class="mining">
                            <div>
                                <div class="mine-top">
                                    <div class="img">
                                        <img src="{{ asset('img/bitcoin-logo.png') }}" alt="bitcoin-logo">
                                    </div>
                                    <div class="text">
                                        <h5>Litecoin ( LTC )</h5>
                                        <p>{!! __('app.calculated_for') !!}<br>1 LTC = $ <span id="spanPriceForLTC">145</span></p>
                                    </div>
                                </div>
                                <form class="mine-left" id="calculator-formLTC">
                                    <div>
                                        <p>{!! __('app.price') !!} ($)</p>
                                        <input id ="inputtedCoinPriceLTC" type="text" value="84">
                                    </div>
                                    <div>
                                        <p>{!! __('app.hashing_power') !!}</p>
                                        <div class="input-group">
                                            <input type="text" id="hashingPowerValueLTC" name="hashingPower" value="4730">
                                            <div class="select-box">
                                                <select id='hashingPowerMeasureLTC'>
                                                    <option value="0.00001">H/s</option>
                                                    <option value="0.01">KH/s</option>
                                                    <option value="0.1">MH/s</option>
                                                    <option value="100">GH/s</option>
                                                    <option value="1000000" selected>TH/s</option>
                                                </select>
                                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <p>{!! __('app.power_consumption') !!} (w)</p>
                                        <input type="text" id="inputtedPowerConsumptionLTC" value="1000">
                                    </div>
                                    <div>
                                        <p>{!! __('app.сost_per') !!} KWh ($)</p>
                                        <input type="text" id="inputtedEnergyCostLTC" value="0.12">
                                    </div>
                                    <div>
                                        <p>{!! __('app.pool_fee') !!} (%)</p>
                                        <input id="inputtedPoolFeeLTC" type="text" value="1">
                                    </div>
                                </form>
                            </div>
                            <div>
                                <div class="mine-main-info">
                                    <div>
                                        <h4 >{!! __('app.profit_ratio_per_day') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitRatioDayLTC">85,143</span>%
                                        </p>
                                    </div>
                                    <div class="text-right">
                                        <h4>{!! __('app.profit_per_month_cap') !!}</h4>
                                        <p class="text-high">
                                            <i class="fa fa-angle-up" aria-hidden="true"></i>
                                            <span id="profitMonthLTC">91090</span>
                                        </p>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.day') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_day') !!}</p>
                                            <span id="profit-dayLTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/day') !!}</p>
                                            <span id="mined-dayLTC" class="text-high">LTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Day') !!}</p>
                                            <span id="energy-cost-dayLTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.week') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_week') !!}</p>
                                            <span id="profit-weekLTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/week') !!}</p>
                                            <span id="mined-weekLTC" class="text-high">LTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/Week') !!}</p>
                                            <span id="energy-cost-weekLTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.month') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_month_low') !!}</p>
                                            <span id="profit-monthLTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/month') !!}</p>
                                            <span id="mined-monthLTC" class="text-high">LTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/month') !!}</p>
                                            <span id="energy-cost-monthLTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mine-info">
                                    <h5><span>{!! __('app.year') !!}</span></h5>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <p>{!! __('app.profit_per_year') !!}</p>
                                            <span id="profit-yearLTC" class="text-high">$ 3,036.43</span>
                                        </div>
                                        <div class="col-xs-4 text-center">
                                            <p>{!! __('app.Mined/year') !!}</p>
                                            <span id="mined-yearLTC" class="text-high">LTC 0.3962</span>
                                        </div>
                                        <div class="col-xs-4 text-right">
                                            <p>{!! __('app.Power_cost/year') !!}</p>
                                            <span id="energy-cost-yearLTC" class="text-high">$ 3.72</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="main-page-block disclaimer">{!! __('app.calculator_disclosure_ltc') !!}</div>
                    </div>
                </div>
                <div class="main-page-block main-news-list main-news-list-mining">
                    <h5>{!! $miningCategory->name !!}</h5>
                    <div class="row">
                        <div class="col-sm-6">
                            <ul>
                                @foreach($miningCategory->posts as $key => $post)
                                    @if(ceil(count($miningCategory->posts)/2) == $key)
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <ul>
                                @endif
                                <li><a href="{{ URL::route('article', ['categorySlug' => $post->category->slug, 'postSlug' => $post->slug]) }}">{{ $post->title }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="buy-filter-fix hidden-xs hidden-sm"></div>
                <h2>{!! __('app.coins_rate') !!}</h2>
                <div class="main-page-block">
                    <ul class="coin-course">
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-low"><i class="fa fa-angle-down" aria-hidden="true"></i>0.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>2.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-low"><i class="fa fa-angle-down" aria-hidden="true"></i>0.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-low"><i class="fa fa-angle-down" aria-hidden="true"></i>0.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-low"><i class="fa fa-angle-down" aria-hidden="true"></i>0.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>2.45%</h5>
                            </div>
                        </li>
                        <li>
                            <div>
                                <h4>BTC</h4>
                                <h5>$7560.9</h5>
                            </div>
                            <div>
                                <p>За год</p>
                                <h5 class="text-high"><i class="fa fa-angle-up" aria-hidden="true"></i>285%</h5>
                            </div>
                            <div>
                                <p>За сутки</p>
                                <h5 class="text-low"><i class="fa fa-angle-down" aria-hidden="true"></i>0.45%</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <h2>{{ __('app.other_news') }}</h2>
                <div class="main-page-block other-news">
                    @foreach($otherPosts as $otherPost)
                        <article>
                            <a href="{{ URL::route('article', ['categorySlug' => $otherPost->category->slug, 'postSlug' => $otherPost->slug]) }}">
                                <div class="img">
                                    <img src="{{ asset('storage').'/'.$otherPost->image }}" alt="{!! $otherPost->title !!}">
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time class="timeago" datetime="{{ $post->created_at }}"></time>
                                    </div>
                                    <div><i class="fa fa-eye" aria-hidden="true"></i>{{ $otherPost->view_count }}</div>
                                    <p>{!! $otherPost->title !!}</p>
                                </div>
                            </a>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <script>
        $().ready(function() {
            var calculator = {
                difficulties : {
                    "BTC": "5178671069072",
                    "ETH": "332091930115351",
                    "ETC": "174755727952585",
                    "XMR": "54657553371",
                    "ZEC": "13041951",
                    "PASC": "7108197",
                    "LTC": "10021799"
                },
                countMinedCoins: function ()
                {
                    var selectedCoinCode = $("#selecetedCoinTab").val();
                    var coinDifficulty = this.difficulties[selectedCoinCode];

                    var hashRate = $("#hashingPowerValue"+selectedCoinCode).val();
                    var hashRateMeasure = $("#hashingPowerMeasure"+selectedCoinCode).val();

                    var coinsPerDay = 86400 / ((coinDifficulty * (2^32)) / (hashRate*hashRateMeasure));
                    var coinsPerWeek = 7*86400 / ((coinDifficulty * (2^32)) / (hashRate*hashRateMeasure));
                    var coinsPerMonth = 30*86400 / ((coinDifficulty * (2^32)) / (hashRate*hashRateMeasure));
                    var coinsPerYear = 365*86400 / ((coinDifficulty * (2^32)) / (hashRate*hashRateMeasure));
                    $("#mined-day"+selectedCoinCode).html(coinsPerDay.toFixed(4));
                    $("#mined-week"+selectedCoinCode).html(coinsPerWeek.toFixed(4));
                    $("#mined-month"+selectedCoinCode).html(coinsPerMonth.toFixed(4));
                    $("#mined-year"+selectedCoinCode).html(coinsPerYear.toFixed(4));

                    var inputtedCoinPrice = $("#inputtedCoinPrice"+selectedCoinCode).val();
                    var inputtedPoolFee = $("#inputtedPoolFee"+selectedCoinCode).val();

                    var inputtedPowerConsumption = $("#inputtedPowerConsumption"+selectedCoinCode).val();
                    var inputtedEnergyCost = $("#inputtedEnergyCost"+selectedCoinCode).val();
                    var energyCostPerDay = inputtedPowerConsumption*inputtedEnergyCost*24;
                    var energyCostPerWeek = inputtedPowerConsumption*inputtedEnergyCost*24*7;
                    var energyCostPerMonth = inputtedPowerConsumption*inputtedEnergyCost*24*30;
                    var energyCostPerYear = inputtedPowerConsumption*inputtedEnergyCost*24*365;
                    $("#energy-cost-day"+selectedCoinCode).html('$ '+this.kFormatter(energyCostPerDay.toFixed(2)));
                    $("#energy-cost-week"+selectedCoinCode).html('$ '+this.kFormatter(energyCostPerWeek.toFixed(2)));
                    $("#energy-cost-month"+selectedCoinCode).html('$ '+this.kFormatter(energyCostPerMonth.toFixed(2)));
                    $("#energy-cost-year"+selectedCoinCode).html('$ '+this.kFormatter(energyCostPerYear.toFixed(2)));

                    var profitPerDay = inputtedCoinPrice*coinsPerDay*(1-inputtedPoolFee/100) - energyCostPerDay;
                    var profitPerWeek = inputtedCoinPrice*coinsPerWeek*(1-inputtedPoolFee/100) - energyCostPerWeek;
                    var profitPerMonth = inputtedCoinPrice*coinsPerMonth*(1-inputtedPoolFee/100) - energyCostPerMonth;
                    var profitPerYear = inputtedCoinPrice*coinsPerYear*(1-inputtedPoolFee/100) - energyCostPerYear;
                    $("#profit-day"+selectedCoinCode).html('$ '+this.kFormatter(profitPerDay.toFixed(2)));
                    $("#profit-week"+selectedCoinCode).html('$ '+this.kFormatter(profitPerWeek.toFixed(2)));
                    $("#profit-month"+selectedCoinCode).html('$ '+this.kFormatter(profitPerMonth.toFixed(2)));
                    $("#profit-year"+selectedCoinCode).html('$ '+this.kFormatter(profitPerYear.toFixed(2)));

                    var minigIncomePerDay = inputtedCoinPrice*coinsPerDay;
                    var miningCostPerDay = (minigIncomePerDay*(inputtedPoolFee/100))+energyCostPerDay;
                    var profitPercentPerDay = ((minigIncomePerDay - miningCostPerDay)/miningCostPerDay)*100;
                    $("#profitRatioDay"+selectedCoinCode).html(profitPercentPerDay.toFixed(3));


                    $("#profitMonth"+selectedCoinCode).html('$ '+this.kFormatter(profitPerMonth.toFixed(3)));

                    calculator.updateColors(profitPerDay);
                },

                updateColors: function (profitAmount) {
                    if (profitAmount >= 0) {
                        $('.mining .text-low').addClass('text-high').removeClass('text-low');
                    } else {
                        $('.mining .text-high').addClass('text-low').removeClass('text-high');
                    }
                },

                kFormatter: function (num) {
                    return num > 999 ? (num/1000).toFixed(1) + 'k' : num
                }

            };

            calculator.countMinedCoins();
            
            var selectedCoinCode = $("#selecetedCoinTab").val();
            $("#inputtedCoinPrice"+selectedCoinCode).on('keyup change', function () {
                calculator.countMinedCoins();
                $("#spanPriceFor"+selectedCoinCode).html($("#inputtedCoinPrice"+selectedCoinCode).val());
            });
            // Select all tabs
            $('.nav-tabs a').click(function(){
                $(this).tab('show');
                $('#selecetedCoinTab').val($(this).data('coin-code'));
                calculator.countMinedCoins();
                var selectedCoinCode = $("#selecetedCoinTab").val();

                $("#inputtedCoinPrice"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                    $("#spanPriceFor"+selectedCoinCode).html($("#inputtedCoinPrice"+selectedCoinCode).val());

                });
                $("#hashingPowerValue"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                });

                $("#hashingPowerMeasure"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                });
                $("#inputtedPoolFee"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                });
                $("#inputtedPowerConsumption"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                });
                $("#inputtedEnergyCost"+selectedCoinCode).on('keyup change', function () {
                    calculator.countMinedCoins();
                });
            })
            //Reward = ((hashrate * block_reward) / current_difficulty) * (1 - pool_fee) * 3600
        });
    </script>
@stop