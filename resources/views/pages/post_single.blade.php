@extends("layout")
@section("body")
    <div class="container news-full-container">
        <ol class="breadcrumb">
            <li><a href="{{ URL::route('home') }}">{{ __('app.homepage') }}</a></li>
            <li><a href="/categories">{{ __('app.library') }}</a></li>
            <li><a href="/categories/{{ $post->category->slug }}">{!! $post->category->name !!}</a></li>
            <li class="active">{!! $post->title !!}</li>
        </ol>
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <article class="news-full">
                    <div class="time-wrapper">
                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                        <time class="timeago" datetime="{{ $post->created_at }}"></time>
                    </div>
                    <ul class="pull-right">
                        <li><i class="fa fa-eye" aria-hidden="true"></i>{{ $post->view_count }}</li>
                    </ul>
                    <div class="news-block">
                        {!! $post->body !!}
                        <div class="sharethis-inline-share-buttons"></div>
                        <script type="text/javascript" src="https://platform-api.sharethis.com/js/sharethis.js#property=5a8c394ce3b02a00133b2cbf&product=inline-share-buttons"></script>
                    </div>
                </article>
                <div id="mc-container"></div>
                <script type="text/javascript">
                    cackle_widget = window.cackle_widget || [];
                    cackle_widget.push({widget: 'Comment', id: 60800});
                    (function() {
                        var mc = document.createElement('script');
                        mc.type = 'text/javascript';
                        mc.async = true;
                        mc.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cackle.me/widget.js';
                        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(mc, s.nextSibling);
                    })();
                </script>
                <a id="mc-link" href="http://cackle.me">Comments system <b style="color:#4FA3DA">Cackl</b><b style="color:#F65077">e</b></a>
            </div>

            <div class="col-md-5 col-lg-4">
                <h2>{{ __('app.other_news') }}</h2>
                <div class="main-page-block other-news">
                    @foreach($newPosts as $newPost)
                        <article>
                            <a href="/categories/{!! $newPost->category->slug !!}/{!! $newPost->slug !!}">
                                <div class="img">
                                    <img src="{{asset('storage')."/".$newPost->image}}" alt="{!! $newPost->title !!}">
                                </div>
                                <div class="text">
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time class="timeago" datetime="{{ $newPost->created_at }}"><i class="fa fa-clock-o" aria-hidden="true"></i></time>
                                    </div>
                                    <div><i class="fa fa-eye" aria-hidden="true"></i>{{ $newPost->view_count }}</div>
                                    <p>{!! $newPost->title !!}</p>
                                </div>
                            </a>
                        </article>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@stop