@extends("layout")
@section("body")
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-lg-8">
                <h2>{{ __('app.found_news') }}</h2>
                <div class="main-page-block">
                    @if(count($searchedPosts) == 0)
                        <h3>Новостей по такому запросу не найдено.</h3>
                        <a href="/categories" class="btn btn-default">Другие новости<i class="fa fa-angle-right"
                                                                                       aria-hidden="true"></i></a>
                    @else
                        @foreach($searchedPosts as $searchedPost)
                            <article class="news-one">
                                <div class="img">
                                    <a href="/categories/{{ $searchedPost->category->slug }}/{{ $searchedPost->slug }}"><img
                                                src="{{ asset('storage').'/'.$searchedPost->image }}"
                                                alt="main-news-1"></a>
                                </div>
                                <div class="text">
                                    <h3>
                                        <a href="/categories/{{ $searchedPost->category->slug }}/{{ $searchedPost->slug }}">{!! $searchedPost->title !!}</a>
                                    </h3>
                                    <p>{!! $searchedPost->excerpt !!}</p>
                                    <div class="time-wrapper">
                                        <i class="fa fa-clock-o" aria-hidden="true"></i>
                                        <time>{{ getTimeAgo($searchedPost->created_at) }}</time>
                                    </div>
                                    <div class="pull-right">
                                        <ul>
                                            <li><i class="fa fa-eye"
                                                   aria-hidden="true"></i>{{ $searchedPost->view_count }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="col-md-5 col-lg-4">
                <h2>{{ __('app.ico_calendar') }}</h2>
                <div class="main-page-block main-ico">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-1" data-toggle="tab">{{ __('app.upcoming') }}</a></li>
                        <li><a href="#tab-2" data-toggle="tab">{{ __('app.current') }}</a></li>
                        <li><a href="#tab-3" data-toggle="tab">{{ __('app.past_ico') }}</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active fade in" id="tab-1">
                            @foreach($sidebarIcos['futureIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-2">
                            @foreach($sidebarIcos['currentIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                        <div class="tab-pane fade" id="tab-3">
                            @foreach($sidebarIcos['pastIcos'] as $currentIco)
                                <a href="{{ route('ico', $currentIco->slug) }}" class="ico-item">
                                    <div class="img">
                                        <img src="{{ asset('storage')."/".$currentIco->icon }}" alt="main-news-1">
                                    </div>
                                    <div class="text">
                                        <h5>{!! $currentIco->name !!}</h5>
                                        <p>{!! __('app.start') !!}
                                            <time>{!! (new DateTime($currentIco->start_time))->format('Y-m-d') !!}</time>
                                        </p>
                                        <p>{!! __('app.end') !!}
                                            <time>{!! (new DateTime($currentIco->end_time))->format('Y-m-d') !!}</time>
                                        </p>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <a href="{{ route('icos') }}" class="btn btn-default">{{ __('app.watch_calendar') }}<i
                                class="fa fa-angle-right"
                                aria-hidden="true"></i></a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <a href="/categories" class="btn btn-default btn-block">{{ __('app.more_news') }}</a>
                </div>
            </div>
        </div>
    </div>
@stop