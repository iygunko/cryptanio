@if ($paginator->hasPages())

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a href="{{ $paginator->nextPageUrl() }}"><button type="button" class="btn btn-default">Next {{$paginator->count()}}<i class="fa fa-angle-double-right" aria-hidden="true"></i></button></a>

        @else
            <button style="color: #3b515c" type="button" disabled="disabled" class="btn btn-disabled">Next {{$paginator->count()}}<i class="fa fa-angle-double-right" aria-hidden="true"></i></button>
        @endif
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <button style="color: #3b515c" type="button" disabled="disabled" class="btn btn-disabled"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev {{$paginator->count()}}</button>
        @else
            <a href="{{ $paginator->previousPageUrl() }}"><button type="button" class="btn btn-default"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev {{$paginator->count()}}</button></a>

        @endif

@endif
