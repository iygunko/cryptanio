<?php

Route::get('test/api1', 'TestController@apiTest1');

Route::get('setlocale/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        Session::put('locale', $locale);
    }
    return redirect()->back();
});

Route::get('categories/{categorySlug}/{postSlug}', 'PostController@index')->name('article');
Route::get('categories/{categorySlug}', 'CategoryController@single')->name('category');
Route::get('categories', 'CategoryController@index')->name('categories');
Route::post('subscribe', 'SubscriberController@addNewSubscriber')->name('subscribe');

Route::get('mining-calculator', 'MiningCalculatorController@index')->name('mining-calculator');

//@TODO validate
Route::get('icos/{icoSlug}', 'IcoController@single')->name('ico');

Route::get('icos', 'IcoController@index')->name('icos');

Route::get('cryptocoins/{cryptocoinSlug}', 'CryptocoinController@single')->name('cryptocoin');
Route::get('cryptocoins', 'CryptocoinController@index')->name('cryptocoins');


Route::get('buy-sell', 'ExchangerController@index')->name('buy-sell');
Route::get('buy-sell', 'ExchangerController@index')->name('buy_sell.do_exchange');

Route::get('search', 'PostController@search')->name('search');


Route::get('/', 'IndexController@index')->name('home');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
